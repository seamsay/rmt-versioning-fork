#!/bin/bash

# Calculate all one- and two- electron integrals needed later.

ln -fs inputs/inp .
ln -fs inputs/h2o2+.molden .

scatci_integrals || exit 1
