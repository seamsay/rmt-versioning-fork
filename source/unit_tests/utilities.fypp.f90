! Copyright 2023
!
! This file is part of RMT.
!
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
!
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
!
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup testing
!> \authors S Marshallsay
!> \date    2023
!>
!> \brief Functions that are useful for writing unit tests.
MODULE testing_utilities
    USE ISO_FORTRAN_ENV, ONLY: INT32, REAL64
    USE precisn, ONLY: wp
    USE rmt_utilities, ONLY: int_to_char

    IMPLICIT NONE

    #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    #!!! IMPORTANT
    #!!! Some of these functions might be easier to understand if you refer to the generated version of this file.
    #!!! The generated version of this file can usually be found at `build/unit_tests/utilities.f90`, but if it's not there try
    #!!! running `find build -name 'utilities.f90'` (if you use a non-standard build folder then search in that folder instead).
    #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    #:set MAX_ARRAY_RANK = 4
    #:set SUPPORTED_TYPES = {'real': ['real64'], 'integer': ['int32'], 'complex': ['real64']}
    #:set SUPPORTED_RANKS = list(range(1, MAX_ARRAY_RANK + 1))

    #:def rankspec(r)
        #:assert r > 0
        :#{for _ in range(r - 1)}#, :#{endfor}#
    #:enddef

    !> \brief An interface for comparing whether two values are equal.
    !>
    !> It simplifies writing check functions for compound types as it provides essentially the same functionality as test-drive's
    !> check functions, but without allocating an error and preventing you from checking other values.
    !>
    !> Use this function in combination with `format_context` to write check functions for compound types, or (if absolutely
    !> necessary) to write test functions that check multiple values. See `format_context` for more information.
    !>
    !> \code
    !> IF (.NOT. compare(actual_foobar, expected_foobar)) THEN
    !>    CALL format_context(context, "foobar", actual_foobar, expected_foobar)
    !> END IF
    !> \endcode
    !>
    !> If you need a new compare function it might make sense to define it in the test file and add it to the interface there. A
    !> general rule of thumb is that if it is an in-built Fortran type (or a derived type that doesn't "belong" to any part of the
    !> codebase) then the new compare function should go in this file but if it is a derived type then it should go in the test file
    !> for the part of the code to which that derived type belongs.
    INTERFACE compare
        #:for type, kinds in SUPPORTED_TYPES.items()
            #:for kind in kinds
                MODULE PROCEDURE compare_${type}$_${kind}$

                #:for rank in SUPPORTED_RANKS
                    MODULE PROCEDURE compare_${type}$_${kind}$_r${rank}$
                    MODULE PROCEDURE compare_${type}$_${kind}$_r${rank}$_subarray
                    MODULE PROCEDURE compare_${type}$_${kind}$_r${rank}$_subarray_list
                #:endfor
            #:endfor
        #:endfor
    END INTERFACE

    ! TODO: For some reason in this interface's documentation (and only this interface's documentation) `compare` doesn't get
    ! autolinked unless it is written as `testing_utilities::compare()`.
    !> \brief An interface for generating failure messages for multiple values.
    !>
    !> This function should be used in conjuction with `testing_utilities::compare()` for writing check functions that check
    !> compound derived types (derived types that contain more than one field) or for writing test functions that test multiple
    !> variables (although this should be avoided where possible).
    !>
    !> Your check (or test) function should contain an allocatable arbitrary length character variable (conventionally called
    !> `context`) that will become the failure message. For each field or variable you need to check, you should call
    !> `testing_utilities::compare()` on it and the expected value, then if `testing_utilities::compare()` returns `.FALSE.` you
    !> should call `format_context`.
    !>
    !> For example, if you have a type
    !> \code
    !> TYPE foo
    !>     INTEGER :: a
    !>     REAL :: b
    !> END TYPE
    !> \endcode
    !> then your check function might look like this:
    !> \code
    !> SUBROUTINE check_foo(error, actual, expected)
    !>     TYPE(error_type), ALLOCATABLE, INTENT(OUT) :: error
    !>     TYPE(foo), INTENT(IN) :: actual, expected
    !>
    !>     CHARACTER(len=:), ALLOCATABLE :: context
    !>
    !>     IF (.NOT. compare(actual%a, expected%a)) THEN
    !>         CALL format_context(context, "a", actual%a, expected%a)
    !>     END IF
    !>
    !>     IF (.NOT. compare(actual%b, expected%b)) THEN
    !>         CALL format_context(context, "b", actual%b, expected%b)
    !>     END IF
    !>
    !>     IF (context /= "") THEN
    !>         CALL test_failed(error, "`foo` type was incorrect!", context)
    !>     END IF
    !> END SUBROUTINE
    !> \endcode
    !>
    !> Alternatively if you need a test function that tests multiple variables it might look like this
    !> \code
    !> SUBROUTINE test_a_and_b(error)
    !>     TYPE(error_type), ALLOCATABLE, INTENT(OUT) :: error
    !>
    !>     INTEGER, PARAMETER :: expected_a = 4
    !>     REAL, PARAMTER :: expected_b = 3.21
    !>     INTEGER :: actual_a
    !>     REAL :: actual_b
    !>
    !>     CHARACTER(len=:), ALLOCATABLE :: context
    !>
    !>     CALL subroutine_that_I_want_to_test(actual_a, actual_b)
    !>
    !>     IF (.NOT. compare(actual_a, expected_a)) THEN
    !>         CALL format_context(context, "a", actual_a, expected_a)
    !>     END IF
    !>
    !>     IF (.NOT. compare(actual_b, expected_b)) THEN
    !>         CALL format_context(context, "b", actual_b, expected_b)
    !>     END IF
    !>
    !>     IF (context /= "") THEN
    !>         CALL test_failed(error, "Some values were incorrect!", context)
    !>     END IF
    !> END SUBROUTINE
    !> \endcode
    !> however code that requires this style of testing is heavily discouraged, instead try to design your code to use derived
    !> types.
    INTERFACE format_context
        #:for type, kinds in SUPPORTED_TYPES.items()
            #:for kind in kinds
                MODULE PROCEDURE format_context_${type}$_${kind}$

                #:for rank in SUPPORTED_RANKS
                    MODULE PROCEDURE format_context_${type}$_${kind}$_r${rank}$
                    MODULE PROCEDURE format_context_${type}$_${kind}$_r${rank}$_subarray
                    MODULE PROCEDURE format_context_${type}$_${kind}$_r${rank}$_subarray_list
                #:endfor
            #:endfor
        #:endfor
    END INTERFACE

    !> An abstraction for comparing some expected data to a subset of actual data.
    !>
    !> \param[in] values The data expected in the subarray.
    !> \param[in] subscript The indices that this `subarray` object represents.
    !> \param[in] full_shape The shape of the original array.
    !>
    !> \attention Currently subarrays only support 1-indexed arrays.
    !>
    !> \remark There are two similar but distinct concepts that are both called subarrays here. The array obtained by indexing into
    !> an array in Fortran (e.g. `foo(2:3, 4)`), and the derived types defined in this file (named `subarray_...` where `...` is the
    !> type of the data stored in them). The former will be referred to as a subarray, while the latter will be referred to as a
    !> `subarray` object.
    !>
    !> A major problem when testing large arrays is that the test files become very large leading to slower compile times, harder to
    !> read tests, and even occasionally hitting compiler-dependent limitations on source code. To get around this the subarray
    !> abstraction allows you to only specify a subset of the expected data and therefore keep file sizes small.
    !>
    !> To create a subarray you can use the functions in this interface. There are three fields to a subarray: the values in this
    !> subarray, the subscript that would have selected this subarray from the original array, and the shape that the original array
    !> should have had. The subscript field uses `testing_utilities::range`, see that type's documentation for more details.
    !>
    !> As a motivating example, let's imagine that you are testing a function that returns an array. We'll say for the sake of
    !> argument that you have saved the result of that function into a variable called `actual` with the type signature
    !> \code
    !> REAL(REAL_64), DIMENSION(118, 47, 2) :: actual
    !> \endcode
    !> If you wanted to test the entire array then you would need to write a static array consisting of 11092 elements to the test
    !> file. Instead you should compare the result against a subarray. If you wanted to just compare the subarray
    !> `actual(34:36, 22:23, 1)` then you should create a `subarray` object like so
    !> \code
    !> TYPE(subarray_real_real64) :: expected
    !> expected = subarray( &
    !>     [1.2_REAL64, 3.2_REAL64, 7.6_REAL64, 8.9_REAL64, 4.4_REAL64, 3.7_REAL64], &
    !>     [range(34, 36), range(22, 23), range(1, 1)], &
    !>     [118, 47, 2] &
    !> )
    !> \endcode
    !> The first argument is the data you expect stored in a flattened format, i.e. as the 1D equivalent of the data. Remember that
    !> Fortran stores arrays in [column-major order](https://en.wikipedia.org/wiki/Row-_and_column-major_order). The second argument
    !> is the subscript that you expect that data to be at, using the format explained in `testing_utilities::range`. And the third
    !> argument is the shape of the original array, used to check that the shape of the actual array is correct.
    !>
    !> To actually test the array you can then use `testing_utilities::compare` and `testing_utilities::format_context`. A full test
    !> function might look like this:
    !> \code
    !> SUBROUTINE test_foo(error)
    !>     TYPE(error_type), ALLOCATABLE, INTENT(OUT) :: error
    !>
    !>     REAL(REAL64), DIMENSION(:, :, :), ALLOCATABLE :: actual
    !>     TYPE(subarray_real_real64) :: expected
    !>     CHACTER(len=:), ALLOCATABLE :: context
    !>
    !>     expected = subarray( &
    !>         [1.2_REAL64, 3.2_REAL64, 7.6_REAL64, 8.9_REAL64, 4.4_REAL64, 3.7_REAL64], &
    !>         [range(34, 36), range(22, 23), range(1, 1)], &
    !>         [118, 47, 2] &
    !>     )
    !>
    !>     actual = foo()
    !>
    !>     IF (.NOT. compare(actual, expected)) THEN
    !>         context = ""
    !>         CALL format_context(context, "foo", actual, expected)
    !>         CALL test_failed(error, "Expected subarray did not match the actual array.", context)
    !>     END IF
    !> END SUBROUTINE
    !> \endcode
    !> In this code snippet `testing_utilities::compare` will correctly compare the data in the `subarray` object with the data in
    !> the subarray `actual(34:36, 22:23, 1)`, and `testing_utilities::format_context` will create a human-readable message showing
    !> which elements (if any) were different (showing the indices of them in the original array), and whether the shape of the
    !> actual array was different to what was expected.
    !>
    !> # Comparing Multiple Subarrays Using A List Of `subarray` Objects
    !>
    !> If you to test multiple sections of an array, `testing_utilities::compare` and `testing_utilities::format_context` will also
    !> work correctly with 1D arrays of `subarray` objects. If we say that you wanted to test both `actual(34:36, 22:23, 1)` and
    !> `actual(1:2, 1:2, 1:2)` in the function above, you could use it like so:
    !> \code
    !> SUBROUTINE test_foo(error)
    !>     TYPE(error_type), ALLOCATABLE, INTENT(OUT) :: error
    !>
    !>     REAL(REAL64), DIMENSION(:, :, :), ALLOCATABLE :: actual
    !>     TYPE(subarray_real_real64), DIMENSION(:), ALLOCATABLE :: expected
    !>     CHACTER(len=:), ALLOCATABLE :: context
    !>     INTEGER, DIMENSION(:), PARAMETER :: expected_shape = [118, 47, 2]
    !>
    !>     expected = [ &
    !>         subarray( &
    !>             [8.8_REAL64, 7.7_REAL64, 6.6_REAL64, 5.5_REAL64, 4.4_REAL64, 3.3_REAL64, 2.2_REAL64, 1.1_REAL64], &
    !>             [range(1, 2), range(1, 2), range(1, 2)], &
    !>             expected_shape &
    !>         ), &
    !>         subarray( &
    !>             [1.2_REAL64, 3.2_REAL64, 7.6_REAL64, 8.9_REAL64, 4.4_REAL64, 3.7_REAL64], &
    !>             [range(34, 36), range(22, 23), range(1, 1)], &
    !>             expected_shape &
    !>         ) &
    !>     ]
    !>
    !>     actual = foo()
    !>
    !>     IF (.NOT. compare(actual, expected)) THEN
    !>         context = ""
    !>         CALL format_context(context, "foo", actual, expected)
    !>         CALL test_failed(error, "Expected subarray did not match the actual array.", context)
    !>     END IF
    !> END SUBROUTINE
    !> \endcode
    !>
    !> \remark If the subarrays are not disjoint, this will lead to shared elements being checked and possibly printed twice.
    !>
    !> # How Should I Pick A Subarray?
    !>
    !> There's no hard and fast rule for how to pick which secions of the array to test, but as a general rule of thumb subarrays
    !> should be between 10 and 30 elements to keep source code lean. If there's a physical reason why a certain section of the
    !> array would be more interesting or more likely to go wrong then that would be a good place to test, but otherwise try
    !> plotting the array (or parts of it) and look for features that stand out.
    INTERFACE subarray
        #:for type, kinds in SUPPORTED_TYPES.items()
            #:for kind in kinds
                MODULE PROCEDURE make_subarray_${type}$_${kind}$
            #:endfor
        #:endfor
    END INTERFACE

    !> A derived type that represents a range in array indexing.
    !>
    !> A range consists of the lower index in the scubscript (the `lower` field) and the upper index in the upper index in the
    !> subscript (the `upper` field). Both of these fields are inclusive, just as normal Fortran indices are.
    !>
    !> For example, the subscript in the expression `my_array(-1:4, 3:7, 10:12)` could be respresented as
    !> \code
    !> [range(-1, 4), range(3, 7), range(10, 12)]
    !> \endcode
    TYPE range
        INTEGER :: lower, upper
    END TYPE

    #:for type, kinds in SUPPORTED_TYPES.items()
        #:for kind in kinds
            TYPE subarray_${type}$_${kind}$
                ${type}$(${kind}$), DIMENSION(:), ALLOCATABLE :: values
                TYPE(range), DIMENSION(:), ALLOCATABLE :: subscript
                INTEGER, DIMENSION(:), ALLOCATABLE :: full_shape
            END TYPE
        #:endfor
    #:endfor

    CHARACTER(len=*), PARAMETER :: separator = " | "

    PRIVATE

    PUBLIC :: compare, data_file, format_context, int_to_char, range, subarray
    #:for type, kinds in SUPPORTED_TYPES.items()
        #:for kind in kinds
            PUBLIC :: subarray_${type}$_${kind}$
        #:endfor
    #:endfor
CONTAINS
    !> \brief Find the root directory of the RMT project.
    !> \authors S Marshallsay
    !> \date 2023
    FUNCTION project()
        CHARACTER(len=:), ALLOCATABLE :: project

        INTEGER :: i, idx, n
        CHARACTER(len=18), DIMENSION(3) :: expected_path

        ! NOTE: Can't use an array literal because the strings are different lengths.
        expected_path(1) = "source"
        expected_path(2) = "unit_tests"
        expected_path(3) = "utilities.fypp.f90"

        project = "${_FILE_}$"

        DO i = SIZE(expected_path), 1, -1
            n = LEN(project)
            idx = INDEX(project, "/", back=.TRUE.)

            IF (idx < 1) ERROR STOP "Unknown error finding project root."
            IF (project(idx + 1:n) /= TRIM(expected_path(i))) ERROR STOP "Project directory structure changed."

            project = project(1:idx - 1)
        END DO
    END FUNCTION

    !> \brief Find the absolute path to a given test data file.
    !> \authors S Marshallsay
    !> \date 2023
    !>
    !> The argument to this function is relative to the `tests` directory in the project root.
    !> So for example, if you wanted to find the path to the `H` file of the `Ne_4cores` test you would use
    !> \code
    !> path = data_file("atomic_tests/small_tests/Ne_4cores/inputs/H")
    !> \endcode
    FUNCTION data_file(file)
        CHARACTER(len=*), INTENT(in) :: file
        CHARACTER(len=:), ALLOCATABLE :: data_file

        data_file = project()//"/tests/"//file
    END FUNCTION

    #! `compare` functions will often need different interfaces/implementations based on their type. For example, reals can't be
    #! compared by exact equality they must instead use a threshold. Therefore we need to handle the types separately.

    #:for kind in SUPPORTED_TYPES['integer']
        PURE ELEMENTAL FUNCTION compare_integer_${kind}$(actual, expected) RESULT (equal)
            INTEGER(${kind}$), INTENT(in) :: actual, expected
            LOGICAL :: equal

            equal = actual == expected
        END FUNCTION
    #:endfor

    #:for kind in SUPPORTED_TYPES['real']
        PURE ELEMENTAL FUNCTION compare_real_${kind}$(actual, expected, rel_tol, abs_tol) RESULT (equal)
            REAL(${kind}$), INTENT(IN) :: actual, expected
            REAL(${kind}$), INTENT(IN), OPTIONAL :: rel_tol, abs_tol
            LOGICAL :: equal

            REAL(${kind}$) :: threshold, rel_tol_inner

            IF (PRESENT(rel_tol)) THEN
                rel_tol_inner = rel_tol
            ELSE
                rel_tol_inner = 1.0E-6_${kind}$
            END IF

            threshold = ABS(expected)*rel_tol_inner
            IF (PRESENT(abs_tol)) threshold = MAX(threshold, abs_tol)

            equal = ABS(actual - expected) <= threshold
        END FUNCTION
    #:endfor

    #:for kind in SUPPORTED_TYPES['complex']
        PURE ELEMENTAL FUNCTION compare_complex_${kind}$(actual, expected, rel_tol, abs_tol) RESULT (equal)
            COMPLEX(${kind}$), INTENT(IN) :: actual, expected
            REAL(${kind}$), INTENT(IN), OPTIONAL :: rel_tol, abs_tol
            LOGICAL :: equal

            REAL(${kind}$) :: threshold, rel_tol_inner

            IF (PRESENT(rel_tol)) THEN
                rel_tol_inner = rel_tol
            ELSE
                rel_tol_inner = 1.0E-6_${kind}$
            END IF

            threshold = ABS(expected)*rel_tol_inner
            IF (PRESENT(abs_tol)) threshold = MAX(threshold, abs_tol)

            equal = ABS(actual - expected) <= threshold
        END FUNCTION
    #:endfor

    #:for type, kinds in SUPPORTED_TYPES.items()
        #:for kind in kinds
            #:for rank in SUPPORTED_RANKS
                PURE FUNCTION compare_${type}$_${kind}$_r${rank}$(actual, expected) RESULT (equal)
                    ${type}$(${kind}$), DIMENSION(${rankspec(rank)}$), INTENT(IN) :: actual, expected
                    LOGICAL :: equal

                    IF (SIZE(actual) /= SIZE(expected)) THEN
                        equal = .FALSE.
                    ELSE
                        equal = ALL(compare_${type}$_${kind}$(actual, expected))
                    END IF
                END FUNCTION

                PURE FUNCTION compare_${type}$_${kind}$_r${rank}$_subarray(actual, expected) RESULT (equal)
                    ${type}$(${kind}$), DIMENSION(${rankspec(rank)}$), INTENT(IN) :: actual
                    TYPE(subarray_${type}$_${kind}$), INTENT(IN) :: expected
                    LOGICAL :: equal

                    IF (ANY(SHAPE(actual) /= expected%full_shape)) THEN
                        equal = .FALSE.
                    ELSE
                        equal = ALL(compare_${type}$_${kind}$( &
                            RESHAPE( &
                                actual( &
                                    #:for r in range(1, rank + 1)
                                        expected%subscript(${r}$)%lower:expected%subscript(${r}$)%upper#{if r != rank}#,#{endif}# &
                                    #:endfor
                                ), &
                                [SIZE(expected%values)] &
                            ), &
                            expected%values &
                        ))
                    END IF
                END FUNCTION

                PURE FUNCTION compare_${type}$_${kind}$_r${rank}$_subarray_list(actual, expected) RESULT(equal)
                    ${type}$(${kind}$), DIMENSION(${rankspec(rank)}$), INTENT(IN) :: actual
                    TYPE(subarray_${type}$_${kind}$), DIMENSION(:), INTENT(IN) :: expected
                    LOGICAL :: equal

                    INTEGER :: i

                    equal = .TRUE.
                    DO i = 1,SIZE(expected)
                        IF (.NOT. compare(actual, expected(i))) THEN
                            equal = .FALSE.
                            RETURN
                        END IF
                    END DO
                END FUNCTION
            #:endfor
        #:endfor
    #:endfor

    #:for kind in SUPPORTED_TYPES['integer']
        SUBROUTINE format_context_integer_${kind}$(context, name, actual, expected)
            CHARACTER(len=:), ALLOCATABLE, INTENT(inout) :: context
            CHARACTER(len=*), INTENT(in) :: name
            INTEGER(${kind}$), INTENT(in) :: actual, expected

            IF (context /= "") THEN
                context = context//" | "
            END IF

            context = context//name//" = "//int_to_char(actual)//" /= "//int_to_char(expected)
        END SUBROUTINE
    #:endfor

    #:for kind in SUPPORTED_TYPES['real']
        SUBROUTINE format_context_real_${kind}$(context, name, actual, expected)
            CHARACTER(len=:), ALLOCATABLE, INTENT(inout) :: context
            CHARACTER(len=*), INTENT(in) :: name
            REAL(${kind}$), INTENT(in) :: actual, expected

            CHARACTER(len=13) :: str_repr

            IF (context /= "") THEN
                context = context//" | "
            END IF

            context = context//name//" = "

            WRITE (str_repr, '(ES13.6)') actual
            context = context//str_repr

            context = context//" /= "

            WRITE (str_repr, '(ES13.6)') expected
            context = context//str_repr
        END SUBROUTINE
    #:endfor

    #:for kind in SUPPORTED_TYPES['complex']
    SUBROUTINE format_context_complex_${kind}$(context, name, actual, expected)
        CHARACTER(len=:), ALLOCATABLE, INTENT(inout) :: context
        CHARACTER(len=*), INTENT(in) :: name
        COMPLEX(${kind}$), INTENT(in) :: actual, expected

        CHARACTER(len=30) :: str_repr

        IF (context /= "") THEN
            context = context//" | "
        END IF

        context = context//name//" = "

        WRITE (str_repr, '(2(1x,ES13.6))') REAL(actual), IMAG(actual)
        context = context//str_repr

        context = context//" /= "

        WRITE (str_repr, '(2(1x,ES13.6))') REAL(expected), IMAG(expected)
        context = context//str_repr
    END SUBROUTINE
#:endfor

    #:for type, kinds in SUPPORTED_TYPES.items()
        #:for kind in kinds
            #:for rank in SUPPORTED_RANKS
                #! A lot of the logic would be shared between the array and subarray versions of `format_context` if we had
                #! completely separate procedures for them, so instead we treat arrays as subarrays that include the entire array.
                SUBROUTINE format_context_array_subarray_${type}$_${kind}$_r${rank}$( &
                    context, &
                    name, &
                    actual_subarray, &
                    actual_shape, &
                    expected_subarray, &
                    expected_shape &
                )
                    CHARACTER(len=:), ALLOCATABLE, INTENT(INOUT) :: context
                    CHARACTER(len=*), INTENT(IN) :: name
                    ${type}$(${kind}$), DIMENSION(${rankspec(rank)}$), INTENT(IN) :: actual_subarray, expected_subarray
                    INTEGER, DIMENSION(${rank}$), INTENT(IN) :: actual_shape, expected_shape

                    ${type}$(${kind}$) :: actual_value, expected_value
                    INTEGER :: i1#{for d in range(2, rank + 1)}#, i${d}$#{endfor}#

                    #:for d in range(1, rank + 1)
                        #:for first, second, remark in [("expected", "actual", "not expected"), ("actual", "expected", "missing")]
                            IF (${first}$_shape(${d}$) < ${second}$_shape(${d}$)) THEN
                                IF (context /= "") context = context//separator
                                context = context// &
                                    "name(#{for _ in range(1, d)}#:, #{endfor}#"// &
                                    int_to_char(${first}$_shape(${d}$) + 1)//":"//int_to_char(${second}$_shape(${d}$))// &
                                    "#{for _ in range(d + 1, rank + 1)}#, :#{endfor}#)"// &
                                    "${remark}$"
                            END IF
                        #:endfor
                    #:endfor

                    #:for d in range(1, rank + 1)
                        DO i${d}$ = 1,MIN(SIZE(actual_subarray, ${d}$), SIZE(expected_subarray, ${d}$))
                    #:endfor
                            actual_value = actual_subarray(i1#{for d in range(2, rank+1)}#, i${d}$#{endfor}#)
                            expected_value = expected_subarray(i1#{for d in range(2, rank+1)}#, i${d}$#{endfor}#)
                            IF (.NOT. compare(actual_value, expected_value)) THEN
                                CALL format_context( &
                                    context, &
                                    name//"(" &
                                        #:for d in range(1, rank)
                                            //int_to_char(i${d}$)//", " &
                                        #:endfor
                                        //int_to_char(i${rank}$)//")", &
                                    actual_value, &
                                    expected_value &
                                )
                            END IF
                    #:for d in range(1, rank + 1)
                        END DO
                    #:endfor
                END SUBROUTINE

                SUBROUTINE format_context_${type}$_${kind}$_r${rank}$(context, name, actual, expected)
                    CHARACTER(len=:), ALLOCATABLE, INTENT(inout) :: context
                    CHARACTER(len=*), INTENT(in) :: name
                    ${type}$(${kind}$), DIMENSION(${rankspec(rank)}$), INTENT(in) :: actual, expected

                    CALL format_context_array_subarray_${type}$_${kind}$_r${rank}$( &
                        context, &
                        name, &
                        actual, &
                        SHAPE(actual), &
                        expected, &
                        SHAPE(expected) &
                    )
                END SUBROUTINE

                SUBROUTINE format_context_${type}$_${kind}$_r${rank}$_subarray(context, name, actual, expected)
                    CHARACTER(len=:), ALLOCATABLE, INTENT(inout) :: context
                    CHARACTER(len=*), INTENT(in) :: name
                    ${type}$(${kind}$), DIMENSION(${rankspec(rank)}$), INTENT(in) :: actual
                    TYPE(subarray_${type}$_${kind}$), INTENT(IN) :: expected

                    ${type}$(${kind}$), DIMENSION(${rankspec(rank)}$), ALLOCATABLE :: actual_subarray, expected_subarray
                    INTEGER, DIMENSION(${rank}$) :: actual_ubounds, expected_shape

                    IF (RANK(actual) /= SIZE(expected%full_shape)) THEN
                        ERROR STOP "Actual array has rank "// &
                            int_to_char(RANK(actual))// &
                            " but the subarray has rank "// &
                            int_to_char(SIZE(expected%full_shape))// &
                            "(indicated by the length of the `subscript` and `full_shape` fields)."
                    END IF

                    actual_ubounds = MIN(expected%subscript%upper, SHAPE(actual))
                    actual_subarray = actual( &
                        #:for d in range(1, rank)
                            expected%subscript(${d}$)%lower:actual_ubounds(${d}$), &
                        #:endfor
                        expected%subscript(${rank}$)%lower:actual_ubounds(${d}$) &
                    )

                    expected_shape = 1 + expected%subscript%upper - expected%subscript%lower
                    expected_subarray = RESHAPE(expected%values, expected_shape)

                    CALL format_context_array_subarray_${type}$_${kind}$_r${rank}$( &
                        context, &
                        name, &
                        actual_subarray, &
                        SHAPE(actual), &
                        expected_subarray, &
                        expected%full_shape &
                    )
                END SUBROUTINE

                SUBROUTINE format_context_${type}$_${kind}$_r${rank}$_subarray_list(context, name, actual, expected)
                    CHARACTER(len=:), ALLOCATABLE, INTENT(inout) :: context
                    CHARACTER(len=*), INTENT(in) :: name
                    ${type}$(${kind}$), DIMENSION(${rankspec(rank)}$), INTENT(in) :: actual
                    TYPE(subarray_${type}$_${kind}$), DIMENSION(:), INTENT(IN) :: expected

                    INTEGER :: i

                    DO i = 1,SIZE(expected)
                        CALL format_context_${type}$_${kind}$_r${rank}$_subarray( &
                            context, &
                            name, &
                            actual, &
                            expected(i) &
                        )
                    END DO
                END SUBROUTINE
            #:endfor
        #:endfor
    #:endfor

    SUBROUTINE assert_subscript_conforms_to_shape(subscript, shape, actual_n_values)
        TYPE(range), DIMENSION(:), INTENT(IN) :: subscript
        INTEGER, DIMENSION(:), INTENT(IN) :: shape
        INTEGER, INTENT(IN) :: actual_n_values

        INTEGER :: i, expected_n_values

        expected_n_values = 1

        IF (size(subscript) /= size(shape)) THEN
             ERROR STOP "Subarray rank ("// &
                 int_to_char(size(subscript))// &
                 ") does not match full array rank("// &
                 int_to_char(size(shape))// &
                 ")"
        END IF

        DO i = 1, SIZE(subscript)
            IF (subscript(i)%lower < 1) THEN
                ERROR STOP "Dimension "// &
                    int_to_char(i)// &
                    " has lower subscript less than 1: "// &
                    int_to_char(subscript(i)%lower)
            END IF
            IF (subscript(i)%upper > shape(i)) THEN
                ERROR STOP "Dimension "// &
                    int_to_char(i)// &
                    " has upper subscript greater than dimension size ("// &
                    int_to_char(shape(i))// &
                    "): "// &
                    int_to_char(subscript(i)%upper)
            END IF

            expected_n_values = expected_n_values * (1 + subscript(i)%upper - subscript(i)%lower)
        END DO

        IF (expected_n_values /= actual_n_values) THEN
            ERROR STOP "Size indicated by subscript ("// &
                int_to_char(expected_n_values)// &
                ") does not match the number of values given ("// &
                int_to_char(actual_n_values)// &
                ")"
        END IF
    END SUBROUTINE

    #:for type, kinds in SUPPORTED_TYPES.items()
        #:for kind in kinds
            FUNCTION make_subarray_${type}$_${kind}$(values, subscript, full_shape) RESULT (array)
                ${type}$(${kind}$), DIMENSION(:), INTENT(IN) :: values
                TYPE(range), DIMENSION(:), INTENT(IN) :: subscript
                INTEGER, DIMENSION(:), INTENT(IN) :: full_shape
                TYPE(subarray_${type}$_${kind}$) :: array

                CALL assert_subscript_conforms_to_shape(subscript, full_shape, SIZE(values))

                array = subarray_${type}$_${kind}$(values, subscript, full_shape)
            END FUNCTION
        #:endfor
    #:endfor
END MODULE testing_utilities
