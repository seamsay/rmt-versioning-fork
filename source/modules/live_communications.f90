! Copyright 2018
!
! This file is part of RMT.
!
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
!
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
!
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief Performs parallel operations in inner-region. Sets up links
!> between dipole coupled symmetry blocks, and contains routines for performing
!> the hamiltonian-wavefunction multiplication operation in parallel across the multilayer
!> parallelised blocks.

MODULE live_communications

    USE precisn,               ONLY: wp
    USE rmt_assert,            ONLY: assert
    USE distribute_hd_blocks2, ONLY: numrows, numrows_sum, numrows_m, numrows_blocks, &
                                     mv_disp, &
                                     mv_counts, &
                                     loc_diag_els
    USE initial_conditions,    ONLY: numsols => no_of_field_confs
    USE mpi_communications,    ONLY: mpi_comm_region
    USE mpi_layer_lblocks,     ONLY: Lb_comm, i_am_block_master, lb_size, inner_region_rank
    USE readhd,                ONLY: max_L_block_size
    USE initial_conditions,    ONLY: req_array_dim
    USE MPI

    IMPLICIT NONE

    INTEGER, ALLOCATABLE, SAVE :: no_vec1_init(:), no_vec2_init(:), no_vec3_init(:)

    INTEGER, ALLOCATABLE, SAVE :: no_vecup(:), no_vecdown(:), no_vecsame(:)


    COMPLEX(wp), ALLOCATABLE, SAVE :: my_vec1s(:, :, :), my_vec2s(:, :, :), my_vec3s(:, :, :)
    INTEGER, ALLOCATABLE, SAVE :: my_blocksize1s(:), my_blocksize2s(:), my_blocksize3s(:)

    INTEGER, SAVE :: ml  ! DDAC: TEMPORARY
    

    PUBLIC parallel_matrix_vector_multiply
    PUBLIC parallel_matrix_vector_multiply_no_field
    PUBLIC psi_Z_psi_multiplication_inner
    PUBLIC gram_schmidt_orthog_lanczos
    PUBLIC order0_pes_sum_vecs
    PUBLIC get_global_pop_inner
    PUBLIC sum_and_distribute_total_beta
    PUBLIC setup_block_links

CONTAINS

!-----------------------------------------------------------------------
! This routine should only be called by L block masters
!-----------------------------------------------------------------------

    SUBROUTINE setup_block_links ! Molecular

        USE mpi_layer_lblocks,  ONLY: Lb_m_rank, my_num_LML_blocks, my_LML_block_id
        USE initial_conditions, ONLY : debug
        USE readhd,            ONLY: dipole_coupled, &
                                     nocoupling, &
                                     downcoupling, &
                                     samecoupling, &
                                     upcoupling, &
                                     no_of_LML_blocks

        IMPLICIT NONE

        INTEGER :: i, j, i_block, block_id, err
        INTEGER :: cnt, ivec1, ivec2, ivec3 ! GSJA: added


        ! Each task constructs a table containing the wavefunction symmetries
        ! that every block master needs to perform the matrix*vector multiplication.
        ! The table contains also the diagonal couplings (if present) which
        ! do not have to be sent accross to other processes.

        IF (i_am_block_master) THEN
           ALLOCATE (no_vecup(my_num_LML_blocks), no_vecdown(my_num_LML_blocks), &
                     no_vecsame(my_num_LML_blocks), stat=err)
           ALLOCATE (no_vec1_init(my_num_LML_blocks), no_vec2_init(my_num_LML_blocks), &
                     no_vec3_init(my_num_LML_blocks), stat=err)
           CALL assert (err == 0, 'allocation of no_vec_<etc> in setup_block_links')
           
           DO i_block = 1, my_num_LML_blocks
               block_id = my_LML_block_id + i_block - 1
               ivec1 = 0
               ivec2 = 0
               ivec3 = 0

               DO i = 1, no_of_LML_blocks !finast
                   DO j = 1, no_of_LML_blocks !finast
                      cnt = dipole_coupled(j, i)
                      IF (cnt /= nocoupling) THEN
                        IF (i .NE. j .AND. debug) PRINT *, i, 'requires wf from Lb_m_rank', j - 1
                        ! GSJA: added count of no of vec(u/s/d)
                        IF (block_id .EQ. j) THEN
                            IF (IAND(cnt, upcoupling) /= 0) ivec1 = ivec1 + 1     !>Was originally Down
                            IF (IAND(cnt, downcoupling) /= 0) ivec2 = ivec2 + 1 !>was originally up
                            IF (IAND(cnt, samecoupling) /= 0) ivec3 = ivec3 + 1
                        END IF
                      END IF
                   END DO
               END DO

               no_vecup(i_block) = ivec1
               no_vecdown(i_block) = ivec2
               no_vecsame(i_block) = ivec3

               no_vec1_init(i_block) = no_vecup(i_block)
               no_vec2_init(i_block) = no_vecdown(i_block)
               no_vec3_init(i_block) = no_vecsame(i_block)

!               CALL allocate_my_vecs  ! GSJA: added ! MP: but not used

               IF (debug) THEN
                   WRITE (*, *) 'rank,no_vec1_init=', Lb_m_rank, no_vec1_init
                   WRITE (*, *) 'rank,no_vec2_init=', Lb_m_rank, no_vec2_init
                   WRITE (*, *) 'rank,no_vec3_init=', Lb_m_rank, no_vec3_init
               END IF
           END DO
        END IF

    END SUBROUTINE setup_block_links

!-----------------------------------------------------------------------

    SUBROUTINE parallel_matrix_vector_multiply(field_real, vecin, vecout, t_step2, dipole_output, vecout2)

        USE distribute_hd_blocks2, ONLY: loc_dblock_u, &
                                         loc_dblock_d, &
                                         loc_dblock_s, &
                                         loc_vblock_u, &
                                         loc_vblock_d, &
                                         loc_vblock_s
        USE global_data,           ONLY: one
        USE mpi_layer_lblocks,     ONLY: Lb_m_rank, my_num_LML_blocks, my_LML_block_id, &
                                         lb_m_comm, istep_v
        USE wall_clock,            ONLY: update_time_test_a1, update_time_test_a2, update_time_test_a3, &
                                         update_time_test_a4, update_time_test_a5 
        USE initial_conditions,    ONLY: timings_desired 

        IMPLICIT NONE

        REAL(wp),    INTENT(IN)              :: field_real(3, numsols)
        COMPLEX(wp), INTENT(IN)              :: vecin(numrows_sum, numsols)
        REAL(wp),    INTENT(IN)              :: t_step2
        COMPLEX(wp), INTENT(OUT)             :: vecout(numrows_sum, numsols)
        COMPLEX(wp), INTENT(OUT), OPTIONAL   :: vecout2(numrows_sum, numsols)
        LOGICAL,     INTENT(IN), OPTIONAL    :: dipole_output
        LOGICAL                              :: dipole_velocity_output
        INTEGER                              :: ii, ierr, isol
        INTEGER                              :: n_up(my_num_LML_blocks), n_down(my_num_LML_blocks), &
                                                n_same(my_num_LML_blocks)
        INTEGER                              :: blocksizeup_aux, blocksizedown_aux, blocksizesame_aux
        INTEGER                              :: blocksize_up(3, my_num_LML_blocks)
        INTEGER                              :: blocksize_down(3, my_num_LML_blocks)
        INTEGER                              :: blocksize_same(3, my_num_LML_blocks)
        COMPLEX(wp)                          :: colvecin(max_L_block_size, numsols, my_num_LML_blocks)
        COMPLEX(wp)                          :: vecup(max_L_block_size, numsols, 3, my_num_LML_blocks), &
                                                vecdown(max_L_block_size, numsols, 3, my_num_LML_blocks), &
                                                vecsame(max_L_block_size, numsols, 3, my_num_LML_blocks)
        COMPLEX(wp)                          :: up_field(numsols, 3, my_num_LML_blocks)
        COMPLEX(wp)                          :: down_field(numsols, 3, my_num_LML_blocks)
        COMPLEX(wp)                          :: same_field(numsols, 3, my_num_LML_blocks)
        INTEGER, DIMENSION(req_array_dim)               :: req_array
        INTEGER                              :: i_temp(3), numrows_v, numrows_cumulative, i_block, my_LML_block
        INTEGER                              :: my_no_vec1_init, my_no_vec2_init, my_no_vec3_init, req_cnt
        integer                              :: i_temp1, i_temp2, numstart(my_num_LML_blocks)   
        real(wp)                             :: vecout_ri(numrows_m, 2*numsols), uds_temp(max_L_block_size), &
                                                vec_uds(max_L_block_size, 2*numsols)
        integer                              :: num1, num2, i_1

        ! Calculate diagonal elements: not required for calculating dipole expectation
        IF (PRESENT(dipole_output))  THEN
!           print *, ' I am', inner_region_rank, ', in dipole_output'
            dipole_velocity_output = dipole_output
            vecout = (0.0_wp, 0.0_wp)
            IF (dipole_velocity_output) THEN
                vecout2 = (0.0_wp, 0.0_wp)
            END IF
        ELSE
            dipole_velocity_output = .FALSE.
            DO isol = 1, numsols
                 vecout(:, isol) = (loc_diag_els(:) * t_step2) * vecin(:, isol)
            END DO
        END IF
        IF (inner_region_rank  == 0 .and. timings_desired) THEN
           call update_time_test_a1
        END IF 

        vecdown = CMPLX(0.0_wp,0.0_wp,wp)
        vecup = CMPLX(0.0_wp,0.0_wp,wp)
        vecsame = CMPLX(0.0_wp,0.0_wp,wp)
        numrows_cumulative = 0 
        do i_block = 1, my_num_LML_blocks
           numstart(i_block) = 1 + numrows_cumulative
           numrows_cumulative = numrows_cumulative + numrows(i_block)
        end do
        
        req_cnt = 0
        req_array=0
        do i_block = 1, my_num_LML_blocks
           numrows_cumulative = numstart(i_block) - 1
           numrows_v = numrows(i_block)
           colvecin = (0.0_wp, 0.0_wp)

        ! Put vecin from all cores in this symmetry block into colvecin
           IF (lb_size > 1) THEN
              DO isol = 1, numsols
                  CALL MPI_GATHERV(vecin(:, isol), numrows_sum, MPI_DOUBLE_COMPLEX, colvecin(:, isol, i_block), &
                                mv_counts, mv_disp, MPI_DOUBLE_COMPLEX, 0, Lb_comm, ierr)
              END DO
           ELSE
              colvecin(1:numrows_v,:, i_block) = vecin(numrows_cumulative+1:numrows_cumulative+numrows_v,:) 
           END IF

           my_LML_block = my_LML_block_id + i_block - 1

           my_no_vec1_init = 0
           my_no_vec2_init = 0
           my_no_vec3_init = 0
           blocksize_down(:,i_block) = 0
           blocksize_up(:,i_block) = 0
           blocksize_same(:,i_block) = 0
           IF (i_am_block_master) THEN
              my_no_vec1_init = no_vec1_init(i_block)
              my_no_vec2_init = no_vec2_init(i_block)
              my_no_vec3_init = no_vec3_init(i_block)

              ! Send/Recv colvecin to/from neighbouring L blocks
              CALL send_recv_master_vecs_ptp_part_1(i_block, my_LML_block, numrows_v, my_no_vec1_init, &
                                          my_no_vec2_init, my_no_vec3_init, colvecin, vecin, numstart, &
                                          vecup, vecdown, vecsame, blocksize_up(:,i_block), &
                                          blocksize_down(:,i_block), blocksize_same(:,i_block), &
                                          n_up(i_block), n_down(i_block), n_same(i_block), field_real, &
                                          up_field(:,:,i_block), down_field(:,:,i_block), &
                                          same_field(:,:,i_block), req_array, req_cnt)
           END IF
        END DO
        IF (inner_region_rank  == 0 .and. timings_desired) THEN
           CALL update_time_test_a2
        END IF 

        IF (i_am_block_master) THEN
           CALL MPI_WAITALL(req_cnt, req_array(1:req_cnt), MPI_STATUSES_IGNORE, ierr)
           IF (inner_region_rank  == 0 .and. timings_desired) THEN
              CALL update_time_test_a3
           END IF 
           CALL mpi_barrier(lb_m_comm, ierr)  ! belt and braces, could be removed
           IF (inner_region_rank  == 0 .and. timings_desired) THEN
              CALL update_time_test_a4
           END IF 
        END IF

        numrows_cumulative = 0
        vecout_ri = 0.0_wp
        DO i_block = 1, my_num_LML_blocks
           numrows_cumulative = numstart(i_block) - 1
           numrows_v = numrows(i_block)
           my_LML_block = my_LML_block_id + i_block - 1
           IF (i_am_block_master) THEN
              my_no_vec1_init = no_vec1_init(i_block)
              my_no_vec2_init = no_vec2_init(i_block)
              my_no_vec3_init = no_vec3_init(i_block)
              ! this routine completes the operations from send_recv_master+vecs_ptp.
              ! In fact it 'multiples by the field'. It could be renamed.
              CALL send_recv_master_vecs_ptp_part_2(my_no_vec1_init, my_no_vec2_init, my_no_vec3_init, &
                                 vecup(:,:,:,i_block), vecdown(:,:,:,i_block), vecsame(:,:,:,i_block), &
                                 n_up(i_block), n_down(i_block), n_same(i_block), &
                                 up_field(:,:,i_block), down_field(:,:,i_block), same_field(:,:,i_block) )
           END IF

           IF (lb_size > 1) THEN
        ! All child tasks must know the length of the vectors received. If n_. > 0 then
        ! we need to use that vector to multiply with the corresponding dipole block.
              IF (i_am_block_master) THEN  
                 i_temp(1) = n_down(i_block)
                 i_temp(2) = n_same(I_block)
                 i_temp(3) = n_up(i_block)
              ELSE
                 i_temp = -99999
              END IF
              CALL MPI_BCAST(i_temp, 3, MPI_INTEGER, 0, Lb_comm, ierr)              
              n_down(i_block) = i_temp(1)
              n_same(i_block) = i_temp(2)
              n_up(i_block) = i_temp(3)
           END IF 


        ! If available use the u,d,s wavefunctions to multiply with the corresponding dipole blocks.
           IF (n_down(i_block) > 0) THEN

               IF (lb_size > 1) then
                  CALL MPI_BCAST(vecdown(:,:,:,i_block), 3*numsols*max_L_block_size, MPI_DOUBLE_COMPLEX, 0, &
                              Lb_comm, ierr)
                  CALL MPI_BCAST(blocksize_down(:,i_block), 3, MPI_INTEGER, 0, Lb_comm, ierr)
               END IF
               DO ii = 1, n_down(i_block) ! GSJA: added
                  blocksizedown_aux = blocksize_down(ii,i_block)
                  DO isol = 1, numsols
                     num1 = (isol - 1) * 2 + 1
                     num2 = num1 + 1
                     DO i_1 = 1, blocksizedown_aux
                        vec_uds(i_1,num1) = REAL(vecdown(i_1,isol,ii,i_block), wp)
                     END DO
                     DO i_1 = 1, blocksizedown_aux
                        vec_uds(i_1,num2) = AIMAG(vecdown(i_1,isol,ii,i_block))
                     END DO
                  END DO
                  CALL DGEMM('n', 'n', numrows_v, 2*numsols, blocksizedown_aux, t_step2, &
                             loc_dblock_d(:,:,i_block), numrows_m, vec_uds, max_L_block_size, 0.0_wp, &
                             vecout_ri, numrows_m)
                  DO isol = 1, numsols
                     num1 = (isol - 1) * 2 + 1
                     num2 = num1 + 1
                     DO i_1 = 1, numrows_v
                        vecout(numrows_cumulative + i_1, isol) = vecout(numrows_cumulative + i_1, isol) + &
                        CMPLX(vecout_ri(i_1,num1), vecout_ri(i_1,num2), wp)
                     END DO
                  END DO    
                  IF (dipole_velocity_output) THEN
                     DO isol = 1, numsols
                        num1 = (isol -1) * 2 + 1
                        num2 = num1 + 1
                        uds_temp(1:blocksizedown_aux) = vec_uds(1:blocksizedown_aux,num1)
                        vec_uds(1:blocksizedown_aux,num1) = - vec_uds(1:blocksizedown_aux,num2)
                        vec_uds(1:blocksizedown_aux,num2) = uds_temp(1:blocksizedown_aux)
                     END DO   
                     CALL DGEMM('n', 'n', numrows_v, 2*numsols, blocksizedown_aux, t_step2, &
                             loc_vblock_d(:,:,i_block), numrows_m, vec_uds, max_L_block_size, 0.0_wp, &
                             vecout_ri, numrows_m)
                     DO isol = 1, numsols
                        num1 = (isol - 1) * 2 + 1
                        num2 = num1 + 1
                        DO i_1 = 1, numrows_v
                           vecout2(numrows_cumulative + i_1, isol) = vecout2(numrows_cumulative + i_1, isol) &
                                + CMPLX(vecout_ri(i_1,num1), vecout_ri(i_1,num2), wp)
                        END DO
                     END DO    
                  END IF
               END DO
           END IF

           IF (n_same(i_block) > 0) THEN
               IF (lb_size > 1) then
                  CALL MPI_BCAST(vecsame(:,:,:,i_block), 3*numsols*max_L_block_size, MPI_DOUBLE_COMPLEX, 0, &
                                 Lb_comm, ierr)
                  CALL MPI_BCAST(blocksize_same(:,i_block), 3, MPI_INTEGER, 0, Lb_comm, ierr)
               END IF 
               DO ii = 1, n_same(i_block)
                  blocksizesame_aux = blocksize_same(ii,i_block)
                  DO isol = 1, numsols
                     num1 = (isol -1) * 2 + 1
                     num2 = num1 + 1
                     DO i_1 = 1, blocksizesame_aux
                        vec_uds(i_1,num1) = REAL(vecsame(i_1,isol,ii,i_block), wp)
                     END DO
                     DO i_1 = 1, blocksizesame_aux
                        vec_uds(i_1,num2) = AIMAG(vecsame(i_1,isol,ii,i_block))
                     END DO
                  END DO   
                  CALL DGEMM('n', 'n', numrows_v, 2*numsols, blocksizesame_aux, t_step2, &
                             loc_dblock_s(:,:,i_block), numrows_m, vec_uds, max_L_block_size, 0.0_wp, &
                             vecout_ri, numrows_m)
                  DO isol = 1, numsols
                     num1 = (isol - 1) * 2 + 1
                     num2 = num1 + 1
                     DO i_1 = 1, numrows_v
                        vecout(numrows_cumulative + i_1, isol) = vecout(numrows_cumulative + i_1, isol) + &
                        CMPLX(vecout_ri(i_1,num1), vecout_ri(i_1,num2), wp)
                     END DO
                  END DO    
                  IF (dipole_velocity_output) THEN
                     DO isol = 1, numsols
                        num1 = (isol -1) * 2 + 1
                        num2 = num1 + 1
                        uds_temp(1:blocksizesame_aux) = vec_uds(1:blocksizesame_aux,num1)
                        vec_uds(1:blocksizesame_aux,num1) = - vec_uds(1:blocksizesame_aux,num2)
                        vec_uds(1:blocksizesame_aux,num2) = uds_temp(1:blocksizesame_aux)
                     END DO   
                     CALL DGEMM('n', 'n', numrows_v, 2* numsols, blocksizesame_aux, t_step2, &
                            loc_vblock_s(:,:,i_block), numrows_m, vec_uds, max_L_block_size, 0.0_wp, &
                            vecout_ri, numrows_m)
                     DO isol = 1, numsols
                        num1 = (isol - 1) * 2 + 1
                        num2 = num1 + 1
                        DO i_1 = 1, numrows_v
                           vecout2(numrows_cumulative + i_1, isol) = vecout2(numrows_cumulative + i_1, isol) &
                                + CMPLX(vecout_ri(i_1,num1), vecout_ri(i_1,num2), wp)
                        END DO
                     END DO    
                  END IF
               END DO
           END IF

           IF (n_up(i_block) > 0) THEN
               IF (lb_size > 1) then
                  CALL MPI_BCAST(vecup(:,:,:,i_block), 3*numsols*max_L_block_size, MPI_DOUBLE_COMPLEX, 0, &
                                 Lb_comm, ierr)
                  CALL MPI_BCAST(blocksize_up(:,i_block), 3, MPI_INTEGER, 0, Lb_comm, ierr)
               END IF
               DO ii = 1, n_up(i_block)
                  blocksizeup_aux = blocksize_up(ii,i_block)
                  DO isol = 1, numsols
                     num1 = (isol -1) * 2 + 1
                     num2 = num1 + 1
                     DO i_1 = 1, blocksizeup_aux
                        vec_uds(i_1,num1) = REAL(vecup(i_1,isol,ii,i_block), wp)
                     END DO
                     DO i_1 = 1, blocksizeup_aux
                        vec_uds(i_1,num2) = AIMAG(vecup(i_1,isol,ii,i_block))
                     END DO
                  END DO   
                  CALL DGEMM('n', 'n', numrows_v, 2*numsols, blocksizeup_aux, t_step2, &
                             loc_dblock_u(:,:,i_block), numrows_m, vec_uds, max_L_block_size, 0.0_wp, &
                             vecout_ri, numrows_m)
                  DO isol = 1, numsols
                     num1 = (isol - 1) * 2 + 1
                     num2 = num1 + 1
                     DO i_1 = 1, numrows_v
                        vecout(numrows_cumulative + i_1, isol) = vecout(numrows_cumulative + i_1, isol) + &
                        CMPLX(vecout_ri(i_1,num1), vecout_ri(i_1,num2), wp)
                     END DO
                  END DO    
                  IF (dipole_velocity_output) THEN
                     DO isol = 1, numsols
                        num1 = (isol -1) * 2 + 1
                        num2 = num1 + 1
                        uds_temp(1:blocksizeup_aux) = vec_uds(1:blocksizeup_aux,num1)
                        vec_uds(1:blocksizeup_aux,num1) = - vec_uds(1:blocksizeup_aux,num2)
                        vec_uds(1:blocksizeup_aux,num2) = uds_temp(1:blocksizeup_aux)
                     END DO   
                     CALL DGEMM('n', 'n', numrows_v, 2* numsols, blocksizeup_aux, t_step2, &
                            loc_vblock_u(:,:,i_block), numrows_m, vec_uds, max_L_block_size, 0.0_wp, &
                            vecout_ri, numrows_m)
                     DO isol = 1, numsols
                        num1 = (isol - 1) * 2 + 1
                        num2 = num1 + 1
                        DO i_1 = 1, numrows_v
                           vecout2(numrows_cumulative + i_1, isol) = vecout2(numrows_cumulative + i_1, isol) &
                                + CMPLX(vecout_ri(i_1,num1), vecout_ri(i_1,num2), wp)
                        END DO
                     END DO    
                  END IF
               END DO
           END IF

        END DO
        IF (inner_region_rank  == 0 .and. timings_desired) THEN
           call update_time_test_a5
        END IF 
        

    END SUBROUTINE parallel_matrix_vector_multiply

!-----------------------------------------------------------------------

    SUBROUTINE parallel_matrix_vector_multiply_no_field(vecin, vecout, t_step2)

        IMPLICIT NONE

        COMPLEX(wp), INTENT(IN)     :: vecin(numrows_sum, numsols)
        REAL(wp), INTENT(IN)     :: t_step2
        COMPLEX(wp), INTENT(OUT)    :: vecout(numrows_sum, numsols)
        INTEGER                     :: isol

        ! Calculate diagonal elements
        DO isol = 1, numsols
            vecout(:, isol) = (loc_diag_els(:) * t_step2) * vecin(:, isol)
        END DO

    END SUBROUTINE parallel_matrix_vector_multiply_no_field

!-----------------------------------------------------------------------

    SUBROUTINE psi_Z_psi_multiplication_inner(psi, z_psi, expec_Z_inner)
        USE mpi_layer_lblocks, ONLY: my_num_lml_blocks 

        IMPLICIT NONE

        COMPLEX(wp), INTENT(IN)    :: psi(1:numrows_sum, 1:numsols)
        COMPLEX(wp), INTENT(IN)    :: Z_psi(1:numrows_sum, 1:numsols)
        COMPLEX(wp), INTENT(OUT)   :: expec_Z_inner(1:numsols)
        COMPLEX(wp), EXTERNAL      :: ZDOTC

        INTEGER :: isol

        DO isol = 1, numsols
            expec_Z_inner(isol) = ZDOTC(numrows_sum, psi(:, isol), 1, Z_psi(:, isol), 1)
        END DO

    END SUBROUTINE psi_Z_psi_multiplication_inner

!-----------------------------------------------------------------------

    SUBROUTINE gram_schmidt_orthog_lanczos(jend, mx, vec1in, vec2in, qvecin, redmat_d, redmat_offd)
        USE wall_clock,         ONLY: update_time_test_g1, update_time_test_g2, update_time_test_g3, &
                                      update_time_test_g4, update_time_test_g5, update_time_test_g6
        USE initial_conditions, ONLY: timings_desired

        IMPLICIT NONE

        INTEGER,                  INTENT(IN)    :: jend, mx
        COMPLEX(wp),              INTENT(IN)    :: vec1in(numrows_sum, numsols)
        COMPLEX(wp), ALLOCATABLE, INTENT(INOUT) :: qvecin(:, :, :)  ! numrows x krydim x numsols
        COMPLEX(wp),              INTENT(INOUT) :: redmat_d(mx, numsols), vec2in(numrows_sum, numsols)
        REAL(wp),                 INTENT(INOUT) :: redmat_offd(mx-1, numsols)

        INTEGER                     :: ierr, isol, i
        REAL(wp)                    :: hj1jb(numsols), tot_hj1jb(numsols), p1
        COMPLEX(wp)                 :: hijb(numsols), tot_hijb(numsols)
        REAL(wp)                    :: veci(numrows_sum), sum1, sum2
        REAL(wp)                    :: vecr(numrows_sum)
!       real(wp)                    :: hijb_r(numsols), hijb_i(numsols), tot_hijbr(numsols), tot_hijbi(numsols)
        COMPLEX(wp), EXTERNAL       :: ZDOTC ! BLAS routine
        REAL(wp),    EXTERNAL       :: DDOT  ! BLAS routine    

        DO isol = 1, numsols
            hijb(isol) = ZDOTC(numrows_sum, vec1in(:, isol), 1, vec2in(:, isol), 1)
 !          hijb_r(isol) = REAL(hijb(isol), wp)
 !          hijb_i(isol) = AIMAG(hijb(isol))
        END DO
        IF (inner_region_rank  == 0 .and. timings_desired) THEN
           CALL update_time_test_g1
        END IF 

!       CALL MPI_ALLREDUCE(hijb_r, tot_hijbr, numsols, MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_region, ierr)
!       CALL MPI_ALLREDUCE(hijb_i, tot_hijbi, numsols, MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_region, ierr)
!       do isol = 1, numsols
!          tot_hijb(isol) = CMPLX(tot_hijbr(isol), tot_hijbi(isol), wp)
!       end do   
        CALL MPI_ALLREDUCE(hijb, tot_hijb, numsols, MPI_DOUBLE_COMPLEX, MPI_SUM, mpi_comm_region, ierr)
        IF (inner_region_rank  == 0 .AND. timings_desired) THEN
           CALL update_time_test_g2
        END IF 

        redmat_d(jend, :) = tot_hijb(:)

        DO isol = 1, numsols
            IF (jend == 1) THEN
                CALL ZAXPY(numrows_sum, -redmat_d(jend, isol), vec1in(:, isol), 1, vec2in(:, isol), 1)
            ELSE
                p1 = redmat_offd(jend-1, isol)
                vec2in(:,isol) = vec2in(:,isol) - p1 * qvecin(1:numrows_sum, jend-1, isol)
                CALL ZAXPY(numrows_sum, -redmat_d(jend, isol), qvecin(1:numrows_sum, jend, isol), 1, &
                           vec2in(:, isol), 1)
            END IF
            IF (inner_region_rank  == 0 .and. timings_desired) THEN
               CALL update_time_test_g3
            END IF 
            DO i = 1, numrows_sum
               vecr(i) = REAL(vec2in(i,isol), wp)
               veci(i) = AIMAG(vec2in(i,isol))
            END DO
            sum1 = DDOT(numrows_sum, vecr, 1, vecr, 1)
!            sum1 = DDOT(numrows_sum, vec2in(1,isol), 2, vec2in(1,isol), 2)
            sum2 = DDOT(numrows_sum, veci, 1, veci, 1)
            hj1jb(isol) = sum1 + sum2
        END DO

        CALL MPI_ALLREDUCE(hj1jb, tot_hj1jb, numsols, MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_region, ierr)
        IF (inner_region_rank  == 0 .AND. timings_desired) THEN
           CALL update_time_test_g5
        END IF 

        tot_hj1jb(:) = SQRT(tot_hj1jb(:))

        IF (jend .LT. mx) THEN
            redmat_offd(jend, :) = tot_hj1jb(:)
        END IF

        DO isol = 1, numsols
           p1 = 1.0_wp / tot_hj1jb(isol)
           vec2in(1:numrows_sum,isol) = p1 * vec2in(1:numrows_sum,isol) 
        END DO
        IF (inner_region_rank  == 0 .and. timings_desired) THEN
           call update_time_test_g6
        END IF 

    END SUBROUTINE gram_schmidt_orthog_lanczos

!-----------------------------------------------------------------------

    !> \brief   Send a wave function segment from owner to other rank.
    !> \authors J Benda, A Brown
    !> \date    2018
    !>
    !> This SUBROUTINE can send a provided segment of the wave function owned by one of the MPI processes
    !> to another MPI process, and returns it along with the supplied component of the electric field. The sending
    !> and receiving can be done by the very same MPI process (i.e. i == j), without any danger of a deadlock.
    !> The send/recv pair is entirely non-blocking so all sends/recvs can be  performed without enforcing an order.
    !> In order to allow all send/recvs to complete before processing the received data the routine returns the
    !> request status of the send or recv to be passed to a MPI_Waitall. For multiple LML blocks per task,
    !> if the sending task and receiving task are the same the data is copied from the common stored
    !> wavefunction on the PE, to avoid overwriting of the data to be 'sent' (and also lots of unnecessary isends
    !> and irecvs slowing the calculation). In this cae the 'isend' option just returns and the 'irecv'
    !> option performs the copy. 
    !>
    !> \param my_i_block          The lb_master counting label (array position in 1:my_num_LML_blocks)
    !> \parammy_LML_block        The LML_block label corresponding to my_i_block
    !> \param my_numrows          The number of rows for this block
    !> \param field         Component of the field to multiply the received vector with; used by 'j'.
    !> \param i             Owner of the wave function segment (one-based index of the total symmetry).
    !> \param j             Receiver of the wave function segment (one-based index of the total symmetry).
    !> \param my_vec        Wave function segment controlled by the current MPI process; referenced by process 'i'.
    !> \param vecin               The set of wavefunction segments associated with this PE. (1:numrows_sum,1:numsols)
    !> \param numstart            The starting row of a particular LML wavefunction segment (1:my_num_LML_blocks) 
    !> \param n             Number of vectors in \c my_vecs and sizes in \c my_blocksizes; updated by 'j'.
    !> \param my_vecs       Collection of all wave function segments received by the process 'j'; updated by 'j'.
    !> \param my_blocksizes For all wave function segment in \c my_vecs, size of the segment.
    !> \param nvec          Maximal value of \c ivec, dimension of \c my_vecs and \c my_blocksizes.
    !> \param req           MPI request status for the non-blocking send or receive posted
    !> \param my_field      Updated vector of field components for processing received data after the MPI_Waitall
    !> \param i_yes         set to 1 if an isend or an irecv is performed, 0 if data is copied.  
    !>
    SUBROUTINE send_recv_master_vecs_ptp_cmpt(my_i_block, my_LML_block, my_numrows, field, i, j, my_vec,&
                                              vecin, numstart, n, my_vecs, my_blocksizes, nvec, req, &
                                              my_field, i_yes)

        USE mpi_layer_lblocks, ONLY: Lb_m_rank, &
                                     Lb_m_comm, istep_v
        USE readhd,            ONLY: states_per_LML_block
        USE mpi_layer_lblocks, ONLY: my_num_LML_blocks, my_LML_block_id, master_for_LML_blocks

        IMPLICIT NONE

        INTEGER,     INTENT(IN)    :: my_i_block, my_LML_block  ! lb_master couting label and LML_block label 
        INTEGER,     INTENT(IN)    :: my_numrows                ! number of rows for this block
        INTEGER, INTENT(IN)        :: i, j, nvec
        INTEGER, INTENT(INOUT)     :: n
        INTEGER, INTENT(INOUT)     :: my_blocksizes(3)
        COMPLEX(wp), INTENT(IN)    :: field(numsols)
        COMPLEX(wp), INTENT(IN)    :: my_vec(max_L_block_size, numsols, my_num_LML_blocks)
        COMPLEX(wp), INTENT(IN)    :: vecin(numrows_sum, numsols)
        INTEGER, INTENT(IN)        :: numstart(my_num_LML_blocks)
        COMPLEX(wp), INTENT(INOUT) :: my_vecs(max_L_block_size, numsols, 3, my_num_LML_blocks)
        INTEGER, INTENT(OUT)       :: req
        COMPLEX(wp), INTENT(INOUT) :: my_field(numsols, 3)
        INTEGER, INTENT(out)       :: i_yes   !  was a send or receive performed?

        INTEGER     :: ierr, j_master, i_master, i_block, numrows_v, num_start
        INTEGER     :: status_array(MPI_STATUS_SIZE)  

        ! i-th master queues a non-blocking send of its (i-th) wave function segment
        i_yes = 0
        IF (my_LML_block == i) THEN
           If (my_LML_block_id > j .or. (my_LML_block_id + my_num_LML_blocks) <= j) then 
               j_master = master_for_LML_blocks(j)
               CALL MPI_ISEND(my_vec(:,:,my_i_block), max_L_block_size * numsols, &           
                    MPI_DOUBLE_COMPLEX, j_master, j, Lb_m_comm, req, ierr)
               i_yes = 1
!               CALL MPI_SEND(my_vec(:,:,my_i_block), max_L_block_size * numsols, &
!                    MPI_DOUBLE_COMPLEX, j_master, j, Lb_m_comm, req, ierr)
!               i_yes = 0
           END IF
        END IF

        ! j-th master queues a non-blocking receive for that segment into its
        ! buffer, and stores the relevant parameters for processing that data
        ! after the receive is complete
        IF (my_LML_block == j) THEN
           IF ((my_LML_block_id <= i) .AND. ((my_lml_block_id + my_num_LML_blocks)) > i) THEN
               n = n + 1
               i_block = i - my_LML_block_id + 1
               numrows_v = numrows(i_block) 
               num_start = numstart(i_block)
               my_vecs(1:numrows_v,:, n, my_i_block) = vecin(num_start:num_start+numrows_v-1,:) 
           ELSE
              n = n + 1
              i_master = master_for_LML_blocks(i)
              my_vecs(:,:,n,my_i_block) = CMPLX(1.0_wp,1.0_wp,wp)
              CALL MPI_iRECV(my_vecs(:, :, n, my_i_block), max_L_block_size * numsols, MPI_DOUBLE_COMPLEX, &
                             i_master, j, Lb_m_comm, req, ierr)
              i_yes = 1
!              CALL MPI_RECV(my_vecs(:, :, n, my_i_block), max_L_block_size * numsols, MPI_DOUBLE_COMPLEX, &
!              i_master, j, Lb_m_comm, status_array, ierr)
!              i_yes = 0
           END IF
           my_blocksizes(n) = states_per_LML_block(i)
           my_field(:, n) = field(:)
        END IF

    END SUBROUTINE send_recv_master_vecs_ptp_cmpt

!-----------------------------------------------------------------------

    !> \brief   Communicate segments of the solution vector between processes
    !> \authors Z Masin, G Armstrong, J Benda, A Brown
    !> \date    2017 - 2018
    !>
    !> Block master 'j' needs the wave function segment from 'i' in case of non-zero coupling (j,i).
    !> The segment is stored in \c my_vec1s, \c my_vec2s or \c my_vec3s, depending on the coupling mode
    !> (up, down, same), and multiplied by the corresponding component of the electric field.
    !>
    !> Molecular-specific notes:
    !> - In molecular calculations, some (j,i) pairs can be coupled by more than one component of the
    !>   dipole operator, which means that we need to consider all three coupling modes for each (j,i)
    !>   combination.
    !> - Also, in molecular calculations, dipole operator can yield non-zero matrix elements even
    !>   for diagonal blocks, i.e. (j,j) /= 0. So, if i == j, the block master 'j' will actually send
    !>   to itself. This must be taken in account below to avoid MPI deadlock and is the reason for
    !>   the MPI_Issend - MPI_Recv - MPI_Wait sequence in Send_Recv_Master_Vecs_Ptp_Cmpt.
    !>
    !> \param my_vec          Segment of the wave function controlled by the current MPI process.
    !> \param my_vec1s        Storage for "up" segments needed by this block master.
    !> \param my_vec2s        Storage for "down" segments needed by this block master.
    !> \param my_vec3s        Storage for "same" segments needed by this block master.
    !> \param my_blocksize1s  Number of states in the "up" segment.
    !> \param my_blocksize2s  Number of states in the "down" segment.
    !> \param my_blocksize3s  Number of states in the "same" segment.
    !> \param n_1             On return, number of "up" segments received.
    !> \param n_2             On return, number of "down" segments received.
    !> \param n_3             On return, number of "same" segments received.
    !> \param field_real      Cartesian components of the electric field in this time step.
    !>
    SUBROUTINE send_recv_master_vecs_ptp_part_1(i_block, my_LML_block, my_numrows, &
                                         my_no_vec1_init, my_no_vec2_init, my_no_vec3_init, &
                                         my_vec, vecin, numstart, my_vec1s, my_vec2s, my_vec3s, &
                                         my_blocksize1s, my_blocksize2s, my_blocksize3s, &
                                         n_1, n_2, n_3, field_real, my_up_field, my_down_field, my_same_field, &
                                         req_array, req_cnt)

        USE global_data,        ONLY: zero
        USE coupling_rules,     ONLY: field_factors
        USE mpi_layer_lblocks,  ONLY: Lb_m_rank, my_num_LML_blocks, my_LML_block_id
        USE readhd,             ONLY: dipole_coupled, &
                                      downcoupling, &
                                      samecoupling, &
                                      upcoupling, &
                                      my_no_of_couplings, &
                                      i_couple_to

        IMPLICIT NONE

        INTEGER,     INTENT(IN)  :: i_block, my_LML_block  ! lb_master couting label and LML_block label  
        INTEGER,     INTENT(IN)  :: my_numrows             ! number of rows for this block
        INTEGER,     INTENT(IN)  :: my_no_vec1_init, my_no_vec2_init, my_no_vec3_init
        REAL(wp),    INTENT(IN)  :: field_real(3, numsols)
        COMPLEX(wp), INTENT(IN)  :: my_vec(max_L_block_size, numsols,my_num_LML_blocks)
        COMPLEX(wp), INTENT(IN)  :: vecin(numrows_sum, numsols)
        INTEGER,     INTENT(IN)  :: numstart(my_num_LML_blocks)
        COMPLEX(wp), INTENT(OUT) :: my_vec1s(max_L_block_size, numsols, 3, my_num_LML_blocks), &
                                    my_vec2s(max_L_block_size, numsols, 3, my_num_LML_blocks), &
                                    my_vec3s(max_L_block_size, numsols, 3, my_num_LML_blocks)
        INTEGER,     INTENT(OUT) :: my_blocksize1s(3), &
                                    my_blocksize2s(3), &
                                    my_blocksize3s(3)
        INTEGER,     INTENT(OUT) :: n_1, n_2, n_3
        COMPLEX(wp), INTENT(OUT) :: my_up_field(numsols, 3)
        COMPLEX(wp), INTENT(OUT) :: my_down_field(numsols, 3)
        COMPLEX(wp), INTENT(OUT) :: my_same_field(numsols, 3)
        INTEGER, DIMENSION(req_array_dim), INTENT(INOUT)   :: req_array
        INTEGER, INTENT(INOUT)   :: req_cnt

        INTEGER                  :: i, j, k, l, cnt
        COMPLEX(wp)              :: fieldfactor(3, numsols)
        INTEGER                  :: tmp_req, ierr, isol, i_yes


        ! vec1 = UP
        ! vec2 = DOWN
        ! vec3 = SAME

        n_1 = 0; my_blocksize1s = 0
        n_2 = 0; my_blocksize2s = 0
        n_3 = 0; my_blocksize3s = 0
        my_up_field = zero
        my_down_field = zero
        my_same_field = zero

        DO l = 1, my_no_of_couplings(i_block)
            DO k = 1, my_no_of_couplings(i_block)
                i = i_couple_to(l, i_block)
                j = i_couple_to(k, i_block)

                ! skip (j,i) coupling if the present rank is neither sender, nor receiver
                IF (my_LML_block /= i .AND. my_LML_block /= j) CYCLE

                cnt = dipole_coupled(j, i)

                ! calculate factors to multiply the dipole blocks with (differs for atomic/molecular mode)
                DO isol = 1, numsols
                    CALL field_factors(field_real(:, isol), j, i, fieldfactor(:, isol))
                END DO

                ! Avoid needless MPI communication when the field is zero ACB
                IF ( (IAND(cnt, upcoupling) /= 0) .AND. ANY(fieldfactor(1, :) /= zero) ) THEN
                   CALL send_recv_master_vecs_ptp_cmpt(i_block, my_LML_block, my_numrows, &
                                                       fieldfactor(1, :), i, j, my_vec, vecin, numstart, &
                                                       n_1, my_vec1s, my_blocksize1s, my_no_vec1_init, &
                                                       tmp_req, my_up_field, i_yes)
                    IF (i_yes == 1) THEN
                       req_cnt = req_cnt + 1
                       IF (req_cnt > req_array_dim) CALL assert (.false., &
                            'live_communications, req_cnt is large, reset req_array_dim')
                       req_array(req_cnt) = tmp_req
                    END IF
                END IF

                IF ( (IAND(cnt, downcoupling) /= 0) .AND. ANY(fieldfactor(2, :) /= zero) ) THEN
                    CALL send_recv_master_vecs_ptp_cmpt(i_block, my_LML_block, my_numrows, &
                                                        fieldfactor(2, :), i, j, my_vec, vecin, numstart, &
                                                        n_2, my_vec2s, my_blocksize2s, my_no_vec2_init, &
                                                        tmp_req, my_down_field, i_yes)
                    IF (i_yes == 1) THEN
                       req_cnt = req_cnt + 1
                       IF (req_cnt > req_array_dim) CALL assert (.false., &
                            'live_communications, req_cnt is large, reset req_array_dim')
                       req_array(req_cnt) = tmp_req
                    END IF 
                END IF

                IF ( (IAND(cnt, samecoupling) /= 0) .AND. ANY(fieldfactor(3, :) /= zero) ) THEN
                    CALL send_recv_master_vecs_ptp_cmpt(i_block, my_LML_block, my_numrows, &
                                                        fieldfactor(3, :), i, j, my_vec, vecin, numstart, &
                                                        n_3, my_vec3s, my_blocksize3s, my_no_vec3_init, &
                                                        tmp_req, my_same_field, i_yes)
                    IF (i_yes == 1) THEN
                       req_cnt = req_cnt + 1
                       IF (req_cnt > req_array_dim) CALL assert (.false., &
                            'live_communications, req_cnt is large, reset req_array_dim')
                       req_array(req_cnt) = tmp_req
                    END IF   
                END IF

            END DO !l
        END DO !k

    END SUBROUTINE send_recv_master_vecs_ptp_part_1
    SUBROUTINE send_recv_master_vecs_ptp_part_2(my_no_vec1_init, my_no_vec2_init, my_no_vec3_init, &
                                                my_vec1s, my_vec2s, my_vec3s, &
                                                n_1, n_2, n_3, &
                                                my_up_field, my_down_field, my_same_field)

        IMPLICIT NONE

        INTEGER,     INTENT(IN)    :: my_no_vec1_init, my_no_vec2_init, my_no_vec3_init
        COMPLEX(wp), INTENT(INOUT) :: my_vec1s(max_L_block_size, numsols, my_no_vec1_init), &
                                      my_vec2s(max_L_block_size, numsols, my_no_vec2_init), &
                                      my_vec3s(max_L_block_size, numsols, my_no_vec3_init)
        INTEGER, INTENT(IN )       :: n_1, n_2, n_3
        COMPLEX(wp), INTENT(IN  )  :: my_up_field(numsols, 3)
        COMPLEX(wp), INTENT(IN)    :: my_down_field(numsols, 3)
        COMPLEX(wp), INTENT(IN)    :: my_same_field(numsols, 3)

        ! process the received data by multiplying the vector by the relevant field component
        CALL vector_field_multiplication(n_1, my_up_field, my_vec1s, my_no_vec1_init)
        CALL vector_field_multiplication(n_2, my_down_field, my_vec2s, my_no_vec2_init)
        CALL vector_field_multiplication(n_3, my_same_field, my_vec3s, my_no_vec3_init)

    END SUBROUTINE send_recv_master_vecs_ptp_part_2

!-----------------------------------------------------------------------

    !> \brief   Process wavefunction data by multiplying with the relevant field component
    !> \authors A Brown
    !> \date    2018
    !>
    !> The subroutine multiplies the received wavefunction vectors with the  relevant field components in
    !> preparation for multiplication with the dipole blocks.
    !>
    !> \param n             Number of wavefunction vectors to process
    !> \param my_field      Array of field components corresponding to wavefunction vectors
    !> \param my_vec        Array of wavefunction segments
    !> \param nvecs         Maximal dimension of \c my_vec
    !>
    SUBROUTINE vector_field_multiplication(n, my_field, my_vec, nvecs)
        INTEGER, INTENT(IN)        :: n
        COMPLEX(WP), INTENT(IN)    :: my_field(numsols, 3)
        INTEGER, INTENT(IN)        :: nvecs
        COMPLEX(WP), INTENT(INOUT) :: my_vec(max_L_block_size, numsols, nvecs)
        INTEGER                    :: ii, isol

        DO ii = 1, n
            DO isol = 1, numsols
                my_vec(:, isol, ii) = my_field(isol, ii) * my_vec(:, isol, ii)
            END DO
        END DO

    END SUBROUTINE vector_field_multiplication
!-----------------------------------------------------------------------

    SUBROUTINE order0_pes_sum_vecs(vecin, vecout)

        IMPLICIT NONE

        COMPLEX(wp), INTENT(IN)     :: vecin(numrows_sum, numsols)
        COMPLEX(wp), INTENT(OUT)    :: vecout(numrows_blocks, numsols)
        INTEGER                     :: ierr, isol

        vecout = (0.0_wp, 0.0_wp)

        if (lb_size > 1) then
           DO isol = 1, numsols
            ! Collect all vectors of dim(numrows) and join into vector of dim(max_L_block_size)
            ! -- to get a vector for this symmetry block for this set of propagation orders
               CALL MPI_GATHERV(vecin(:, isol), numrows_sum, MPI_DOUBLE_COMPLEX, vecout(:, isol), &
                               mv_counts, mv_disp, MPI_DOUBLE_COMPLEX, 0, Lb_comm, ierr)
           END DO

        ! Bcast the result to rest of L block
           CALL MPI_BCAST(vecout, max_L_block_size * numsols, MPI_DOUBLE_COMPLEX, 0, Lb_comm, ierr)
        ELSE
           vecout(1:numrows_sum,:) = vecin(1:numrows_sum,:)
        END IF   
           
    END SUBROUTINE order0_pes_sum_vecs

!-----------------------------------------------------------------------

    SUBROUTINE get_global_pop_inner(vecin, tnorm)

        USE mpi_layer_lblocks,  ONLY: my_LML_block_id, &
                                      Lb_m_comm
        USE readhd,             ONLY: states_per_LML_block

        IMPLICIT NONE

        COMPLEX(wp), INTENT(IN)    :: vecin(numrows_blocks, numsols)
        REAL(wp), INTENT(OUT)      :: tnorm(numsols)
        REAL(wp)                   :: norm(numsols)
        INTEGER                    :: ierr, i, isol
        INTEGER                    :: L_block_size
        INTEGER                    :: sum_dummy

        if (lb_size > 1) then
           L_block_size = states_per_LML_block(my_LML_block_id)
           sum_dummy = L_block_size
        else
           sum_dummy = numrows_sum
        end if

        DO isol = 1, numsols
            norm(isol) = 0.0_wp
            DO i = 1, sum_dummy
                norm(isol) = norm(isol) + REAL(CONJG(vecin(i, isol))*vecin(i, isol))
            END DO
        END DO

        CALL MPI_REDUCE(norm, tnorm, numsols, MPI_DOUBLE_PRECISION, MPI_SUM, 0, Lb_m_comm, ierr)

    END SUBROUTINE get_global_pop_inner

!-----------------------------------------------------------------------

    SUBROUTINE get_global_expec_Z_inner(my_local_var, global_var)

        IMPLICIT NONE

        COMPLEX(wp), INTENT(IN)  :: my_local_var
        COMPLEX(wp), INTENT(OUT) :: global_var
        INTEGER :: ierr

        CALL MPI_REDUCE(my_local_var, global_var, 1, &
                        MPI_DOUBLE_COMPLEX, MPI_SUM, 0, mpi_comm_region, ierr)

    END SUBROUTINE get_global_expec_Z_inner

!-----------------------------------------------------------------------

    SUBROUTINE sum_and_distribute_total_beta(vecin_ri, tbeta)

        IMPLICIT NONE

        REAL(wp), INTENT(IN)        :: vecin_ri(numrows_sum, 2*numsols)
        REAL(wp),    INTENT(OUT)    :: tbeta(numsols)
        REAL(wp)                    :: beta(numsols), beta_r, beta_i
        INTEGER                     :: ierr, isol, num1, num2
!        REAL(wp), EXTERNAL          :: DDOT

        DO isol = 1, numsols
            num1 = (isol - 1) * 2 + 1
            num2 = num1 + 1
            beta_r = DOT_PRODUCT(vecin_ri(:, num1), vecin_ri(:, num1))
            beta_i = DOT_PRODUCT(vecin_ri(:, num2), vecin_ri(:, num2))
            beta(isol) = beta_r + beta_i
        END DO
        CALL MPI_ALLREDUCE(beta, tbeta, numsols, MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_region, ierr)

        tbeta = SQRT(tbeta)

    END SUBROUTINE sum_and_distribute_total_beta

!-----------------------------------------------------------------------

    SUBROUTINE gather_C_time_derivs(vecin, vecout)

        IMPLICIT NONE

        COMPLEX(wp), INTENT(IN)     :: vecin(numrows_sum,numsols)
        COMPLEX(wp), INTENT(OUT)    :: vecout(numrows_blocks, numsols)
        INTEGER                     :: ierr, isol

        vecout = (0.0_wp, 0.0_wp)

        ! Collect all vectors of dim(numrows) and join into vector of dim(max_L_block_size)
        ! -- to get a vector for this symmetry block
        IF (lb_size > 1) THEN
           do isol = 1, numsols
              CALL MPI_GATHERV(vecin(:,isol), numrows(1), MPI_DOUBLE_COMPLEX, vecout(:,isol), &
                            mv_counts, mv_disp, MPI_DOUBLE_COMPLEX, 0, Lb_comm, ierr)
           END DO
           CALL MPI_BCAST(vecout, max_L_block_size * numsols, MPI_DOUBLE_COMPLEX, 0, Lb_comm, ierr)
        ELSE
           DO isol = 1, numsols
              vecout(1:numrows_sum,isol) = vecin(1:numrows_sum,isol)
           END DO
        END IF
           
    END SUBROUTINE gather_C_time_derivs

!-----------------------------------------------------------------------

!   SUBROUTINE allocate_my_vecs
!
!       IMPLICIT NONE
!
!       INTEGER :: err
!
!       ALLOCATE (my_vec1s(max_L_block_size, numsols, no_vec1_init), &
!                 my_vec2s(max_L_block_size, numsols, no_vec2_init), &
!                 my_vec3s(max_L_block_size, numsols, no_vec3_init), &
!                 my_blocksize1s(no_vec1_init), my_blocksize2s(no_vec2_init), my_blocksize3s(no_vec3_init), &
!                 stat=err)

!       CALL assert(err .EQ. 0, 'allocation error with vecs')

!       my_vec1s = (0.0_wp, 0.0_wp)
!       my_vec2s = (0.0_wp, 0.0_wp)
!       my_vec3s = (0.0_wp, 0.0_wp)

!       my_blocksize1s = 0
!       my_blocksize2s = 0
!       my_blocksize3s = 0

!   END SUBROUTINE allocate_my_vecs

END MODULE live_communications
