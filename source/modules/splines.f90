! Copyright 2018
!
! This file is part of RMT.
!
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
!
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
!
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief Utility routines for the calculation of splines.
MODULE splines

    USE precisn
    USE file_num, ONLY: file_number

    IMPLICIT NONE

    COMPLEX(wp), ALLOCATABLE :: spl(:, :)
    REAL(wp), ALLOCATABLE    :: splwave(:, :, :)
    REAL(wp), ALLOCATABLE    :: bb(:, :)
    REAL(wp), ALLOCATABLE    :: r(:)
    INTEGER                  :: ndim
    INTEGER                  :: nb

    PRIVATE
    PUBLIC read_spline_files, calc_and_write_wv, bsplvb, interv

CONTAINS

    SUBROUTINE bsplvb(t, nt, jhigh, index, x, left, biatx)
!   From  * A practical guide to splines *  by c. de boor Springer Verlag New
!   York (1978)
!   Calculates the value of all possibly nonzero b-splines at  x  of order
!
!               jout  =  max( jhigh , (j+1)*(index-1) )
!
!    with knot sequence  t .

!   I.E The value of splines at x

        USE calculation_parameters, ONLY: jmax

        IMPLICIT NONE

        INTEGER, INTENT(IN)       :: nt, jhigh, index, left
        REAL(KIND=wp), INTENT(IN) :: x
        REAL(KIND=wp), INTENT(IN), DIMENSION(nt)     :: t
        REAL(KIND=wp), INTENT(OUT), DIMENSION(jhigh) :: biatx

        INTEGER       :: i, jp1, ji
        INTEGER       :: j = 1
        REAL(KIND=wp), DIMENSION(jmax) :: deltal, deltar
        REAL(KIND=wp) :: saved, term, dr1, dr0

        dr1 = 1._wp
        dr0 = 0._wp

        IF (index .NE. 2) THEN
            j = 1
            biatx(1) = dr1
        END IF

        IF ((index .EQ. 2) .OR. (j .LT. jhigh)) THEN

            DO ji = j, jhigh - 1
                jp1 = ji + 1
                deltar(ji) = t(left + ji) - x
                deltal(ji) = x - t(left + 1 - ji)
                saved = dr0
                DO i = 1, ji
                    term = biatx(i)/(deltar(i) + deltal(jp1 - i))
                    biatx(i) = saved + deltar(i)*term
                    saved = deltal(jp1 - i)*term
                END DO
                biatx(jp1) = saved
            END DO
        END IF

    END SUBROUTINE bsplvb

!---------------------------------------------------------------------------

    SUBROUTINE interv(xt, lxt, x, left, mflag)
!   From  * A practical guide to splines *  by c. de boor Springer Verlag New
!   York (1978)
!   Find the interval within t containing x:
!   computes  left = max( i :  xt(i) .lt. xt(lxt) .and.  xt(i) .le. x )  . )

        IMPLICIT NONE

        INTEGER, INTENT(IN)       :: lxt
        INTEGER, INTENT(OUT)      :: left, mflag
        REAL(KIND=wp), INTENT(IN) :: x
        REAL(KIND=wp), INTENT(IN), DIMENSION(lxt) :: xt
        REAL(KIND=wp)             :: dr2
        INTEGER                   :: ihi, ilo, middle, i, n

        dr2 = 2._wp
        ilo = 1
        ihi = lxt
        left = 0
        mflag = 0

        IF ((x .LT. xt(ilo)) .OR. (lxt .LE. 1)) THEN
            left = 1
            mflag = -1
        ELSE
            IF (x .GT. xt(ihi)) THEN
                left = lxt
                mflag = 1
            ELSE
                n = INT(LOG(REAL(lxt - 1))/LOG(dr2) + dr2)
                DO i = 1, n
                    middle = (ihi + ilo)/2
                    IF (middle .EQ. ilo) left = ilo
                    IF (x .LT. xt(middle)) ihi = middle
                    IF (x .GE. xt(middle)) ilo = middle
                END DO
            END IF
        END IF

        IF (left .EQ. 0) THEN
            PRINT *, 'Possible problem in interval'
            STOP
        END IF

    END SUBROUTINE interv

!---------------------------------------------------------------------------

    SUBROUTINE read_spline_files(num_exp_points_inner)
    ! Read the spline basis to build the states from

        USE initial_conditions, ONLY: disk_path, &
                                      numsols => no_of_field_confs
        USE readhd,             ONLY: no_of_L_blocks, &
                                      inast, &
                                      nchmx, &
                                      nstmx, &
                                      kept, &
                                      nstk, &
                                      nchn

        INTEGER, INTENT(IN)   :: num_exp_points_inner
        REAL(wp), ALLOCATABLE :: t(:), values(:)
        INTEGER               :: nco, nt, nc, left, leftmk, mflag
        REAL(wp)              :: xt
        INTEGER               :: kc, k9, i, j

        ALLOCATE (r(num_exp_points_inner))

        OPEN (UNIT=34, FILE=disk_path//'Splinedata', FORM='unformatted')
        READ (34) nc, k9, nb, ndim, nt
        ALLOCATE (t(nt), bb(num_exp_points_inner, nb), values(nb))
        READ (34) t
        CLOSE (34)

        DO i = 1, num_exp_points_inner
            r(i) = (t(nt) - t(1))/REAL(num_exp_points_inner - 1, wp)*(i - 1)
        END DO

        DO i = 1, num_exp_points_inner
            values = 0._wp
            xt = r(i)

            ! Locate x with respect to knot array t.
            CALL interv(T, NB, Xt, LEFT, MFLAG)
            LEFTMK = LEFT - K9
            CALL bsplvb(T, NT, K9, 1, Xt, LEFT, VALUES(LEFTMK + 1))
            BB(I, :) = VALUES(:)
        END DO

        ALLOCATE (splwave(nchmx*ndim, nstmx, no_of_L_blocks), spl(ndim, numsols))

        splwave(:, :, :) = 0.0_wp

        OPEN (UNIT=10, FILE=disk_path//'Splinewaves', FORM='unformatted')
        nco = 0
        kc = 0
        DO i = 1, inast
            IF (kept(i) .EQ. 1) THEN
                kc = kc + 1           !kc keeps track of what the index of kept states is without rearrangement
            END IF

            ! nstk reads in the orig order of ALL symmetries, in line with ordering of splwave
            DO j = 1, nstk(i)
                IF (kept(i) .EQ. 1) THEN
                    READ (10) splwave(1:nchn(kc)*ndim, j, kc)
                ELSE
                    READ (10)
                END IF
            END DO
        END DO
        CLOSE (UNIT=10)

    END SUBROUTINE read_spline_files

!---------------------------------------------------------------------------

    SUBROUTINE calc_and_write_wv(num_exp_points_inner, recon)
    !> \brief Calculate the explicit wavefunction for each channel and write to file
    !> \authors D Clarke, G Armstrong (and others)

        USE initial_conditions, ONLY: numsols => no_of_field_confs

        USE readhd, ONLY: no_of_LML_blocks, &
                          LML_block_nchan, &
                          LML_block_tot_nchan, &
                          LML_block_post, &
                          LML_block_nspn, &
                          LML_block_lrgl, &
                          LML_block_npty, &
                          finind_L, &
                          wf

        IMPLICIT NONE

        INTEGER, INTENT(IN) :: num_exp_points_inner
        LOGICAL, INTENT(IN) :: recon
        INTEGER             :: symind, unitnum
        INTEGER             :: ichco, isol, j, k, ii, jj, kk
        COMPLEX(wp), ALLOCATABLE :: wv(:, :)
        CHARACTER(LEN=:), ALLOCATABLE :: outname

        ALLOCATE (wv(num_exp_points_inner, numsols))

        ichco = 0
        DO j = 1, no_of_LML_blocks
            CALL finind_L(LML_block_nspn(j),LML_block_lrgl(j),LML_block_npty(j),symind)
            DO k = 1, LML_block_nchan(j)

                ichco = ichco + 1
                outname = 'InnerWave' // file_number(ichco, LML_block_tot_nchan)
                spl(:, :) = 0._wp

                DO ii = LML_block_post(j - 1) + 1, LML_block_post(j)
                    DO jj = 1, ndim
                        spl(jj, :) = spl(jj, :) + wf(ii, :) * splwave((k - 1)*ndim + jj, ii - LML_block_post(j - 1), symind)
                    END DO
                END DO

                DO isol = 1, numsols
                    wv(:, isol) = MATMUL(bb(:, 2:nb), spl(1:ndim, isol))
                END DO

                IF (recon) THEN
                    OPEN (NEWUNIT=unitnum, FILE=outname)
 
                    DO kk = 1, num_exp_points_inner
                        WRITE (unitnum, '(*(E25.15))') r(kk), (REAL(wv(kk, isol)), AIMAG(wv(kk, isol)), isol = 1, numsols)
                    END DO

                    CLOSE (unitnum)
                END IF

            END DO
        END DO

    END SUBROUTINE calc_and_write_wv

!---------------------------------------------------------------------------


END MODULE splines
