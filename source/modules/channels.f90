MODULE modChannel
   USE precisn, ONLY: wp
   IMPLICIT NONE

   TYPE Channel
      INTEGER:: Species
      INTEGER:: NQNumbers
      INTEGER, ALLOCATABLE:: QNumber(:)
      CHARACTER(LEN=10), ALLOCATABLE:: Label(:)
      INTEGER:: X_Last
      COMPLEX(wp), ALLOCATABLE:: Wavefunction(:, :)
   END TYPE

   INTEGER, PARAMETER:: LS_Channel_ID = 1
   INTEGER, PARAMETER:: jK_Channel_ID = 2
   INTEGER, PARAMETER:: Uncoupled_Target_ID = 3
   INTEGER, PARAMETER:: DOE_Channel_ID = 4

   PRIVATE

   PUBLIC:: jK_Channel_ID
   PUBLIC:: DOE_Channel_ID
   PUBLIC:: LS_Channel_ID
   PUBLIC:: Uncoupled_Target_ID
   PUBLIC:: Channel
   PUBLIC:: SetupChannel
   PUBLIC:: AppendChannel
   PUBLIC:: AddChannel
   PUBLIC:: ChannelIncluded
   PUBLIC:: PrintAllChannels
   PUBLIC:: AttachWfnToChannel
   PUBLIC:: DetachWfnFromChannel
   PUBLIC:: AllocateWfns
   PUBLIC:: satisfies_triangle_rule

   CONTAINS

   SUBROUTINE AttachWfnToChannel(ChannelsIn, wv)
      TYPE(Channel), INTENT(INOUT), ALLOCATABLE :: channelsIn(:)
      COMPLEX(wp), ALLOCATABLE, INTENT(INOUT) :: wv(:, :, :)
      INTEGER :: npt, nch, isol, ich, nsol

      npt = SIZE(wv, 1)   ! number of finite difference points
      nch = SIZE(wv, 2)   ! number of channels
      nsol = SIZE(wv, 3)   ! number of solutions

      CALL AllocateWfns(ChannelsIn, npt, nsol)
      DO isol = 1, nsol
         DO ich = 1, nch
            ChannelsIn(ich)%Wavefunction(:, isol) = wv(:, ich, isol)
         END DO
      END DO
      DEALLOCATE (wv)
   END SUBROUTINE AttachWfnToChannel

   SUBROUTINE DetachWfnFromChannel(ChannelsIn, wv)
      TYPE(Channel), INTENT(INOUT) :: channelsIn(:)
      COMPLEX(wp), INTENT(INOUT), ALLOCATABLE :: wv(:, :, :)
      INTEGER :: npt, nch, isol, ich, nsol

      nch = ubound(ChannelsIn, 1)
      npt = ubound(ChannelsIn(1)%Wavefunction, 1)
      nsol = ubound(ChannelsIn(1)%Wavefunction, 2)

      ALLOCATE (wv(npt, nch, nsol))
      DO ich = 1, nch
         DO isol = 1, nsol
            wv(:, ich, isol) = ChannelsIn(ich)%Wavefunction(:, isol)
         END DO
         DEALLOCATE (ChannelsIn(ich)%Wavefunction)
      END DO

   END SUBROUTINE detachWfnFromChannel


   !> \brief Add a new channel with the given quantum numbers
   !
   !> Having read the necessary quantum numbers from file (e.g. the H file), add a
   !> new channel in the correct format (LS or JK).
   SUBROUTINE AddChannel(Target_Counter, &
                         l_of_Target, &
                         l_in_current_sym, &
                         Current_L, &
                         ml, &
                         pty, &
                         Current_S, &
                         s_of_target, &
                         coupling_id, &
                         channelsIn)
      USE initial_conditions, ONLY: jK_coupling_id, LS_coupling_id

      INTEGER, INTENT(IN) :: Target_Counter
      INTEGER, INTENT(IN) :: l_of_target
      INTEGER, INTENT(IN) :: l_in_current_sym
      INTEGER, INTENT(IN) :: Current_L
      INTEGER, INTENT(IN) :: ml
      INTEGER, INTENT(IN) :: pty
      INTEGER, INTENT(IN) :: Current_S
      INTEGER, INTENT(IN) :: s_of_target
      INTEGER, INTENT(IN) :: coupling_id
      TYPE(Channel), INTENT(INOUT), ALLOCATABLE :: channelsIn(:)
      TYPE(Channel) :: TempChannel

      INTEGER :: K

      CALL SetupChannel(TempChannel, coupling_id)
      TempChannel%QNumber(1) = Target_Counter
      TempChannel%QNumber(2) = l_of_Target
      TempChannel%QNumber(3) = l_In_Current_Sym

      SELECT CASE (coupling_id)
      CASE (jK_coupling_ID)
         TempChannel%QNumber(5) = Current_L
         TempChannel%QNumber(6) = ml
         TempChannel%QNumber(7) = pty
         TempChannel%X_Last = 0

         ! Try first K
         K = abs(Current_L - 1)
         TempChannel%QNumber(4) = K
         IF ((.not. ChannelIncluded(ChannelsIn, TempChannel)) &
             .AND. &
             Satisfies_Triangle_Rule(K, l_of_Target, l_in_current_sym)) THEN
            CALL AppendChannel(ChannelsIn, TempChannel)
         ELSE
            ! Try second possible K
            K = abs(Current_L + 1)
            TempChannel%QNumber(4) = K
            IF ((.not. ChannelIncluded(ChannelsIn, TempChannel)) &
                .AND. &
                Satisfies_Triangle_Rule(K, l_of_Target, l_in_current_sym)) THEN
               CALL AppendChannel(ChannelsIn, TempChannel)
            END IF

         END IF
      CASE (LS_coupling_id)
         TempChannel%QNumber(4) = Current_L
         TempChannel%QNumber(5) = ml
         TempChannel%QNumber(6) = s_of_target - 1
         TempChannel%QNumber(7) = Current_S - 1
         TempChannel%QNumber(8) = mod(Current_S - 1, 2)
         TempChannel%QNumber(9) = pty
         TempChannel%X_Last = 0
         IF (.not. ChannelIncluded(ChannelsIn, TempChannel)) THEN
            CALL AppendChannel(ChannelsIn, TempChannel)
         END IF
      END SELECT

   END SUBROUTINE AddChannel

   LOGICAL FUNCTION satisfies_triangle_rule(K, ltarg, lsym)
      INTEGER, INTENT(IN) :: K
      INTEGER, INTENT(IN) :: ltarg
      INTEGER, INTENT(IN) :: lsym

      satisfies_triangle_rule = ((K >= abs(ltarg - 2*lsym)) &
                                 .AND. &
                                 (K <= abs(ltarg + 2*lsym)))

   END FUNCTION satisfies_triangle_rule

   SUBROUTINE SetupChannel(ChannelIn, Species_ID)
      TYPE(Channel), INTENT(INOUT):: ChannelIn
      INTEGER, INTENT(IN):: Species_ID

      ChannelIn%Species = Species_ID

      SELECT CASE (Species_ID)

      CASE (LS_Channel_ID)

         ChannelIn%NQNumbers = 9
         ALLOCATE (ChannelIn%QNumber(9))
         ALLOCATE (ChannelIn%Label(9))

         ChannelIn%Label(2) = 'TargetL'
         ChannelIn%Label(3) = 'ElecL'
         ChannelIn%Label(4) = 'TotalL'
         ChannelIn%Label(5) = 'TotalML'
         ChannelIn%Label(6) = 'TargetS'
         ChannelIn%Label(7) = 'TotalS'
         ChannelIn%Label(8) = 'TotalMS'
         ChannelIn%Label(9) = 'Parity'

      CASE (Uncoupled_Target_ID)

         ChannelIn%NQNumbers = 9
         ALLOCATE (ChannelIn%QNumber(9))
         ALLOCATE (ChannelIn%Label(9))

         ChannelIn%Label(2) = 'TargetL'
         ChannelIn%Label(3) = 'TargetML'
         ChannelIn%Label(4) = 'Elecl'
         ChannelIn%Label(5) = 'ElecML'
         ChannelIn%Label(6) = 'TargetS'
         ChannelIn%Label(7) = 'TargetMS'
         ChannelIn%Label(8) = 'ElecMS'
         ChannelIn%Label(9) = 'Parity'

      CASE (jK_Channel_ID)

         ChannelIn%NQNumbers = 7
         ALLOCATE (ChannelIn%QNumber(7))
         ALLOCATE (ChannelIn%Label(7))

         ChannelIn%Label(2) = 'TargetJ'
         ChannelIn%Label(3) = 'ElecL'
         ChannelIn%Label(4) = 'TotalK'
         ChannelIn%Label(5) = 'TotalJ'
         ChannelIn%Label(6) = 'TotalMJ'
         ChannelIn%Label(7) = 'Parity'

      CASE (DOE_Channel_ID)

         ChannelIn%NQNumbers = 8
         ALLOCATE (ChannelIn%QNumber(8))
         ALLOCATE (ChannelIn%Label(8))

         ChannelIn%Label(2) = 'TargetJ'
         ChannelIn%Label(3) = 'TargetMJ'
         ChannelIn%Label(4) = 'ElecL'
         ChannelIn%Label(5) = 'ElecML'
         ChannelIn%Label(6) = 'ElecS'
         ChannelIn%Label(7) = 'ElecMS'
         ChannelIn%Label(8) = 'Parity'

      END SELECT

      ChannelIn%Label(1) = 'TargetN'

   END SUBROUTINE SetupChannel

   SUBROUTINE AppendChannel(ChannelArray, ChannelToAdd)
      TYPE(Channel), INTENT(INOUT), ALLOCATABLE:: ChannelArray(:)
      TYPE(Channel), INTENT(IN):: ChannelToAdd
      TYPE(Channel), ALLOCATABLE:: TempChannelArray(:)
      INTEGER:: Number_of_Channels

      Number_of_Channels = UBound(ChannelArray, 1)

      TempChannelArray = ChannelArray

      DEALLOCATE (ChannelArray)
      ALLOCATE (ChannelArray(Number_of_Channels + 1))

      ChannelArray(:Number_of_Channels) = TempChannelArray
      ChannelArray(Number_of_Channels + 1) = ChannelToAdd

   END SUBROUTINE AppendChannel

   SUBROUTINE AllocateWfns(ChannelArray, Number_of_Points, Number_of_sols)
      TYPE(Channel), INTENT(INOUT), ALLOCATABLE:: ChannelArray(:)
      INTEGER, INTENT(IN):: Number_of_Points, Number_of_sols
      INTEGER:: Channel_Counter
      INTEGER:: Number_of_Channels

      Number_of_Channels = UBound(ChannelArray, 1)

      DO Channel_Counter = 1, Number_of_Channels
         ALLOCATE (ChannelArray(Channel_Counter)%Wavefunction(Number_of_Points, Number_of_sols))
         ChannelArray(Channel_Counter)%Wavefunction(:, :) = 0.0_wp
         ChannelArray(Channel_Counter)%X_Last = Number_of_Points
      END DO

   END SUBROUTINE AllocateWfns

   SUBROUTINE PrintAllChannels(ChannelArray, filename)
      TYPE(Channel), INTENT(IN):: ChannelArray(:)
      CHARACTER(*), INTENT(IN), OPTIONAL :: filename
      CHARACTER(200) :: outputFile
      INTEGER :: Number_of_Channels
      INTEGER :: Channel_Counter
      INTEGER :: QNumber_Counter
      INTEGER :: io

      Number_of_Channels = UBound(ChannelArray, 1)

      IF (PRESENT(filename)) THEN
         outputFile = TRIM(filename)
      ELSE
         outputFile = TRIM("ChannelInfo")
      END IF
      
      open (NEWUNIT=io, FILE=outputFile, STATUS="replace", FORM="formatted")

      Write (io, fmt='(A15)', advance='no') '               '
      DO QNumber_Counter = 1, ChannelArray(1)%NQNumbers
         Write (io, fmt='(A10)', advance='no') ChannelArray(1)%Label(QNumber_Counter)
      END DO
      Write (io, fmt='(A1)', advance='yes') ' '

      DO Channel_Counter = 1, Number_of_Channels
         Write (io, fmt='(I10)', advance='no') Channel_Counter
         DO QNumber_Counter = 1, ChannelArray(1)%NQNumbers
            Write (io, fmt='(I10)', advance='no') ChannelArray(Channel_Counter)%QNumber(QNumber_Counter)
         END DO
         Write (io, fmt='(A1)', advance='yes') ' '
      END DO

   END SUBROUTINE PrintAllChannels

   FUNCTION ChannelIncluded(ChannelArray, ChannelTest)
      TYPE(Channel), INTENT(IN), ALLOCATABLE:: ChannelArray(:)
      TYPE(Channel), INTENT(IN):: ChannelTest
      LOGICAL ChannelIncluded

      INTEGER:: Number_of_Channels
      INTEGER:: Number_QN
      INTEGER:: Channel_Counter
      INTEGER:: QNumber_Counter
      LOGICAL:: Channel_Test

      Number_of_Channels = UBound(ChannelArray, 1)
      Number_QN = ChannelTest%NQNumbers

      ChannelIncluded = .false.

      DO Channel_Counter = 1, Number_of_Channels
         Channel_Test = .true.
         DO QNumber_Counter = 1, Number_QN

            IF (ChannelArray(Channel_Counter)%QNumber(QNumber_Counter) .ne. ChannelTest%QNumber(QNumber_Counter)) THEN
               Channel_Test = .false.
            END IF

         END DO

         IF (Channel_Test) THEN
            ChannelIncluded = .true.
         END IF

      END DO

   END FUNCTION ChannelIncluded

END MODULE modChannel
