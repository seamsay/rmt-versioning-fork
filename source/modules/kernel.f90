! Copyright 2018 
!
! This file is part of RMT.
! 
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
! 
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
! 
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit 
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief High level controller for executing the arnoldi propagation and setting
!> up/sharing data required for it.

MODULE kernel

    USE rmt_assert, ONLY: assert
    USE precisn, ONLY: wp

    IMPLICIT NONE

    COMPLEX(wp), ALLOCATABLE, SAVE, PRIVATE :: psi_inner_on_grid(:, :, :)

CONTAINS

    SUBROUTINE propagation_loop(number_channels, &
                            deltar, &
                            Z_minus_N, &
                            current_field_strength, &
                            timeindex)

        USE initial_conditions,        ONLY: timings_desired, no_of_pes_per_sector
        USE calculation_parameters,    ONLY: nfdm, &
                                             half_fd_order
        USE communications_parameters, ONLY: inner_group_id
        USE initial_conditions,        ONLY: numsols => no_of_field_confs
        USE inner_propagators,         ONLY: propagate_inner
        USE local_ham_matrix,          ONLY: r_value_at_r1st
        USE mpi_communications,        ONLY: first_pes_share_cmplx2_from, &
                                             i_am_inner_master, &
                                             i_am_outer_master, &
                                             i_am_in_outer_region, &
                                             i_am_in_inner_region, &
                                             i_am_outer_rank1, &
                                             check_initial_field, &
                                             timeindex_val, &
                                             i_am_in_first_outer_block, mpi_comm_block, &
                                             i_am_in_0_plus_first_outer_block, share_cmplx_array_among_comm
        USE outer_propagators,         ONLY: propagate_outer
        USE inner_to_outer_interface,  ONLY: share_psi_at_inner_fd_pts_with_first_outer
        USE wall_clock,                ONLY: update_time_start_inner, &
                                             update_outer_time1, &
                                             update_time_end_inner, &
                                             update_outer_time7, &
                                             write_timings_files, &
                                             hel_time, &
                                             update_start_share, update_end_share

        USE wavefunction,              ONLY: expec_outer, &
                                             expec_inner, &
                                             hr

        INTEGER, INTENT(IN)  :: number_channels
        INTEGER, INTENT(IN)  :: timeindex
        REAL(wp), INTENT(IN) :: deltar
        REAL(wp), INTENT(IN) :: Z_minus_N
        REAL(wp), INTENT(IN) :: current_field_strength(3, numsols)

        REAL(wp) :: time_start, time_end
        INTEGER  :: length_of_array, send_pe
        
        IF (timeindex == 0) THEN
            ! Check if all x,y,z EField components are zero for each solution
            CALL check_initial_field(current_field_strength)
        END IF

        time_start = hel_time()
        timeindex_val = timeindex 
        
        IF ((i_am_inner_master) .AND. timings_desired) THEN
            CALL update_time_start_inner(-1)
            CALL update_start_share()
        END IF

        IF (i_am_in_outer_region .AND. timings_desired) THEN
            CALL update_outer_time1(-1)
            CALL update_start_share()
        END IF

        ! Send psi_inner_on_grid from 1st PE in inner region to 1st PE in outer region
        IF (no_of_pes_per_sector == 1) THEN
           CALL first_pes_share_cmplx2_from(inner_group_id, &
                                         psi_inner_on_grid, &
                                         nfdm, &
                                         number_channels*numsols, &
                                         i_am_inner_master, &
                                         i_am_outer_master)
        ELSE IF (i_am_in_0_plus_first_outer_block) THEN
           CALL share_psi_at_inner_fd_pts_with_first_outer(nfdm, numsols, psi_inner_on_grid)
        END IF   
        IF ((i_am_inner_master) .AND. timings_desired) THEN
            CALL update_time_end_inner
            CALL update_end_share 
        END IF

        IF (i_am_in_outer_region .AND. timings_desired) THEN
            CALL update_outer_time7
            CALL update_end_share 
        END IF

!            call get_my_pe_id(my_pe_id)
        IF (i_am_in_outer_region) THEN
           CALL propagate_outer(nfdm, half_fd_order, &
                                 deltar, &
                                 Z_minus_N, &
                                 current_field_strength, &
                                 psi_inner_on_grid, &
                                 r_value_at_r1st, &
                                 i_am_outer_master, i_am_in_first_outer_block, &
                                 hr, &
                                 expec_outer)
        ELSE IF (i_am_in_inner_region) THEN
            CALL propagate_inner(nfdm, &
                                 current_field_strength, &
                                 timeindex, &
                                 i_am_inner_master, &
                                 number_channels,&
                                 expec_inner,&
                                 psi_inner_on_grid)
        END IF

        ! Write timing data:
        IF (timings_desired) THEN
            time_end = hel_time()
            CALL write_timings_files(time_end - time_start, &
                                     i_am_inner_master, &
                                     i_am_outer_master, &
                                     i_am_outer_rank1)
        END IF

    END SUBROUTINE propagation_loop

!-----------------------------------------------------------------------

    SUBROUTINE initialise_psi_inner_on_grid(number_channels)

        USE calculation_parameters,    ONLY: nfdm
        USE initial_conditions,        ONLY: numsols => no_of_field_confs
        USE inner_propagators,         ONLY: update_psi_inner_on_grid
        USE mpi_communications,        ONLY: i_am_in_inner_region

        INTEGER, INTENT(IN) :: number_channels
        INTEGER             :: err

        ALLOCATE (psi_inner_on_grid(nfdm, number_channels, numsols), stat=err)
        CALL assert(err .EQ. 0, 'allocation error with psi_inner_on_grid')

        psi_inner_on_grid = (0.0_wp, 0.0_wp)

        IF (i_am_in_inner_region) THEN
            CALL update_psi_inner_on_grid(psi_inner_on_grid, nfdm, number_channels, numsols)
        END IF

    END SUBROUTINE initialise_psi_inner_on_grid

!-----------------------------------------------------------------------

    SUBROUTINE deallocate_psi_inner_on_grid

        INTEGER :: err

        DEALLOCATE (psi_inner_on_grid, stat=err)
        CALL assert(err .EQ. 0, 'deallocation error with psi_inner_on_grid')

    END SUBROUTINE deallocate_psi_inner_on_grid

END MODULE kernel
