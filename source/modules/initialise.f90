! Copyright 2018 
!
! This file is part of RMT.
! 
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
! 
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
! 
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit 
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief High level controller for setup of calculation. Calls routines to setup
!> communications, grid, timing, checkpointing, io, data arrays and to read in
!> calculation data.

MODULE initialise

    USE initial_conditions, ONLY: propagation_order, &
                                  deltar
    USE mpi_communications, ONLY: i_am_outer_master, &
                                  i_am_inner_master, &
                                  i_am_in_first_outer_block, &
                                  i_am_in_last_outer_block, &
                                  i_am_in_outer_region, &
                                  i_am_in_double_outer_region, &
                                  i_am_in_inner_region, &
                                  my_block_id
    use mpi_layer_lblocks,  only: my_num_LML_blocks
    USE rmt_assert,         ONLY: assert
    USE precisn,            ONLY: wp

    IMPLICIT NONE

CONTAINS

    SUBROUTINE initialise_everything(r_at_region_bndry, &
                                     Z_minus_N, &
                                     number_channels, my_number_channels, &
                                     my_post_dim_lim, my_post_for_tdse, &
                                     start_of_run_timeindex, &
                                     stage)
   
        USE calculation_parameters, ONLY: half_fd_order, &
                                          nfdm, &
                                          krylov_hdt_desired, &
                                          delta_t
        USE grid_parameters,        ONLY: init_channel_id_last, my_channel_id_1st, my_channel_id_last, &
                                          channel_id_1st, channel_id_last
        USE initial_conditions,     ONLY: molecular_target, dipole_velocity_output
        USE inner_parallelisation,  ONLY: deallocate_my_surf_amps
        USE io_files,               ONLY: open_output_files
        USE kernel,                 ONLY: initialise_psi_inner_on_grid
        USE mpi_communications,     ONLY: init_communications_module_part_one, &
                                          i_am_outer_rank1, get_my_pe_id, &
                                          set_pe_gs_id_and_get_comm_gs_plus_first_outer
        USE readhd,                 ONLY: init_read_in_and_distribute, &
                                          nfdm_molecular, &
                                          deltar_molecular, &
                                          r_points
        USE mpi_layer_lblocks,      ONLY: i_am_gs_master, pe_gs_id
        use communications_parameters, ONLY: pe_id_1st_inner
        USE setup_bspline_basis,    ONLY: setup_IR_basis_for_fd_points
        USE stages,                 ONLY: broadcast_stage, &
                                          stage_first
        USE wall_clock,             ONLY: start_program_clock
        USE wavefunction,           ONLY: setup_wavefunction_module


        REAL(wp), INTENT(OUT) :: r_at_region_bndry
        REAL(wp), INTENT(OUT) :: Z_minus_N
        INTEGER, INTENT(OUT)  :: number_channels, my_number_channels  ! total and distributed number, outer region
        INTEGER, INTENT(IN)   :: my_post_dim_lim
        INTEGER, INTENT(OUT)  :: my_post_for_tdse(my_post_dim_lim)
        INTEGER, INTENT(OUT)  :: start_of_run_timeindex
        INTEGER, INTENT(OUT)  :: stage

        LOGICAL  :: nfdm_ok, deltar_ok
        INTEGER  :: i, pe_gs_id_loc, my_pe_id
        REAL(wp) :: spoint

        ! Do only parts of mpi initialisation that are needed to broadcast
        ! parameters (i.e. mpi_init, and find my_rank_id).
        ! The rest of the mpi initialisation is completed
        ! in init_communications_module_part_two
        CALL init_communications_module_part_one

        CALL init_parameters

        CALL init_modules

        IF (molecular_target .AND. dipole_velocity_output) THEN
            dipole_velocity_output = .FALSE.
            IF (i_am_inner_master) THEN
                PRINT *, '=================================================================='
                PRINT *, "Dipole velocity routines not yet implemented for molecular targets"
                PRINT *, "              Calculation will proceed without them               "
                PRINT *, '              =====================================               '
            END IF
        END IF

        ! Inner_Master_Reads_Input_Files reads H, D and Splines files generated by R-matrix codes

!        call get_my_pe_id(my_pe_id)
        CALL init_read_in_and_distribute(i_am_inner_master, &
                                         i_am_in_inner_region, &
                                         r_at_region_bndry, &
                                         number_channels, &
                                         Z_minus_N)

        
        IF (i_am_inner_master .AND. .NOT. molecular_target) THEN

            ! Set up B-Spline basis to calculate inner region wavefunction at grid points near the
            !    inter-region boundary
            CALL setup_IR_basis_for_fd_points(deltar, nfdm)

        END IF

        IF (i_am_inner_master .AND. molecular_target) THEN

            nfdm_ok = .true.
            IF (nfdm .NE. nfdm_molecular) THEN
                nfdm_ok = .false.
                PRINT *, ' nfdm = ', nfdm, ' nfdm_molecular = ', nfdm_molecular
            END IF

            CALL assert(nfdm_ok, 'nfdm error: check values of propagation_order and use_two_inner_grid_pts')

            deltar_ok = .true.
            IF (deltar .NE. deltar_molecular) THEN
                deltar_ok = .false.
                PRINT *, ' deltar = ', deltar, ' deltar_molecular = ', deltar_molecular
            END IF

            CALL assert(deltar_ok, 'Check value of DeltaR!')


            !This check follows the set-up of the inner region points in the atomic code routine Setup_IR_Basis_For_FD_Points.
            spoint = r_at_region_bndry - (REAL(nfdm, wp)*deltar)
            CALL r_point_check(spoint, r_points(1))
            DO i = 2, nfdm
                spoint = spoint + deltar
                CALL r_point_check(spoint, r_points(i))
            END DO

            PRINT *, ' Passed consistency check for the finite difference points.'

        END IF

        CALL init_channel_id_last(number_channels)
        if (i_am_in_outer_region) then
! sets local channels for no_of_pes_per_sector > 1
           call init_my_channels(number_channels, my_number_channels) 
        else if (i_am_in_inner_region) then
           my_number_channels = number_channels ! belt and braces check
           my_channel_id_1st = channel_id_1st
           my_channel_id_last = channel_id_last
           ! inner master communicates channel data with first outer block
           if (i_am_inner_master) call init_my_channels(number_channels, my_number_channels)
        end if   
           
        CALL init_regions(my_post_dim_lim, my_post_for_tdse, r_at_region_bndry)

        CALL broadcast_stage(start_of_run_timeindex, stage)

        ! sort out ground state communicator
        !and arrays for data sharing with first outer region if no_of_pes_per_sector > 1 
        CALL get_my_pe_id(my_pe_id)
        pe_gs_id_loc = 0
        IF (i_am_in_inner_region) THEN
            IF (i_am_gs_master) THEN
                pe_gs_id_loc = my_pe_id
            END IF
        END IF
        call set_pe_gs_id_and_get_comm_gs_plus_first_outer(pe_gs_id_loc, pe_gs_id)
        IF (my_pe_id == pe_gs_id) THEN
           IF (pe_gs_id /= pe_id_1st_inner) then
              CALL init_my_channels(number_channels, my_number_channels) 
           END IF
        END IF        
        
        CALL setup_wavefunction_module(stage, &
                                       stage_first, &
                                       nfdm, &
                                       krylov_hdt_desired, &
                                       delta_t, &
                                       propagation_order, &
                                       half_fd_order)

        IF (i_am_in_inner_region) CALL deallocate_my_surf_amps

        ! gets wavefunction up to rmatr-dr - as needed for propagation:
        CALL initialise_psi_inner_on_grid(my_number_channels)

        CALL open_output_files(stage, my_block_id, my_num_LML_blocks, i_am_inner_master, i_am_outer_master, &
                               i_am_outer_rank1)

        IF (i_am_inner_master) CALL start_program_clock

    END SUBROUTINE initialise_everything

!-----------------------------------------------------------------------

    SUBROUTINE init_parameters

        USE communications_parameters, ONLY: derive_communication_parameters
        USE calculation_parameters,    ONLY: derive_calculation_parameters
        USE grid_parameters,           ONLY: derive_grid_parameters
        USE initial_conditions,        ONLY: read_initial_conditions, &
                                             get_disk_path
        USE initial_conditions,        ONLY: no_of_pes_to_use_outer, no_of_pes_per_sector
        
        INTEGER           ::  rem
        
        CALL get_disk_path('')

        !All tasks read parameter file
        CALL read_initial_conditions

        CALL derive_calculation_parameters

        rem = MOD(no_of_pes_to_use_outer, no_of_pes_per_sector)
        if (rem /= 0) call assert (.false., &
             'For the moment number of pes_per_sector should be a factor of num_pes_in_outer_region')
        CALL derive_communication_parameters

        CALL derive_grid_parameters

    END SUBROUTINE init_parameters

!-----------------------------------------------------------------------

    SUBROUTINE init_modules

        USE coordinate_system,      ONLY: print_coordinate_system
        USE calculation_parameters, ONLY: half_fd_order, &
                                          krylov_hdt_desired, &
                                          delta_t
        USE electric_field,         ONLY: init_electric_field_module
        USE grid_parameters,        ONLY: init_grid_parameters_module
        USE io_parameters,          ONLY: init_io_parameters
        USE kryspace_taylor_sums,   ONLY: init_kryspace_taylor_sums_mod
        USE mpi_communications,     ONLY: init_communications_module_part_two, &
                                          print_hostname
        USE stages,                 ONLY: init_stages
        USE version_control,        ONLY: print_program_header

        CALL init_communications_module_part_two

        IF (i_am_inner_master) THEN
            CALL print_program_header
            CALL print_hostname
            CALL print_coordinate_system
        END IF

        CALL init_grid_parameters_module(i_am_in_first_outer_block, &
                                         half_fd_order, &
                                         propagation_order, &
                                         i_am_in_outer_region)

        CALL init_io_parameters

        CALL init_stages

        CALL init_kryspace_taylor_sums_mod(delta_t, krylov_hdt_desired)

        CALL init_electric_field_module

    END SUBROUTINE init_modules

!-----------------------------------------------------------------------

    SUBROUTINE init_my_channels(number_channels, my_number_channels)
        USE initial_conditions, ONLY : no_of_pes_per_sector, debug
        USE mpi_communications, ONLY : get_my_block_group_pe_id, i_am_outer_master, &
                                       i_am_inner_master, block_comm_channel_remainder, &
                                       disp_for_pe, counts_per_pe
        USE mpi_communications, ONLY :  get_my_group_pe_id, get_my_pe_id
        USE grid_parameters,    ONLY : channel_id_1st, channel_id_last 
        USE grid_parameters,    ONLY : my_channel_id_1st, my_channel_id_last, &
                                       my_num_channels, pe_for_channel
        
        INTEGER, INTENT(IN)  :: number_channels
        INTEGER, INTENT(OUT) :: my_number_channels
        
        INTEGER              :: i_start, i_fin, i_pe, rem, rem2, my_block_group_pe_id, ierr, my_pe_id  

        if (no_of_pes_per_sector > number_channels) call assert (.false., &
             'no_of_pes_per_sector is larger than number_channels for this run. Please reconsider')
        rem = MOD(number_channels, no_of_pes_per_sector)
        block_comm_channel_remainder = rem
        if (i_am_in_outer_region) then
           ALLOCATE (disp_for_pe(0:no_of_pes_per_sector), &
                counts_per_pe(0:no_of_pes_per_sector), stat=ierr)
           call assert (ierr == 0, 'allocation problem in init_my_channels')
           my_number_channels = number_channels / no_of_pes_per_sector  ! add 1 to first rem pes below
           call get_my_block_group_pe_id(my_block_group_pe_id)
           i_start = 1
           disp_for_pe(0) = 0
           counts_per_pe(0) = 0
! disp_for_pe nd counts_per_pe are used in the 'first inner pe + first outer block' communicator           
           i_fin = 0
           do i_pe = 0, rem - 1
              disp_for_pe(i_pe+1) = i_start - 1
              counts_per_pe(i_pe+1) = my_number_channels + 1
              i_fin = i_start + my_number_channels
              if (i_pe == my_block_group_pe_id) then
                 my_channel_id_1st = i_start
                 my_channel_id_last = i_fin
              end if
              pe_for_channel(i_start:i_fin) = i_pe    ! pe_for channel is used in the outer region only
              i_start = i_fin + 1
           end do   
           do i_pe = rem, no_of_pes_per_sector - 1
              i_fin = i_start + my_number_channels - 1
              disp_for_pe(i_pe+1) = i_start - 1
              counts_per_pe(i_pe+1) = my_number_channels
              if (i_pe == my_block_group_pe_id) then
                 my_channel_id_1st = i_start
                 my_channel_id_last = i_fin
              end if
              pe_for_channel(i_start:i_fin) = i_pe   ! pe_for channel is used in the outer region only
              i_start = i_fin + 1
           end do   
           if (my_block_group_pe_id < rem) my_number_channels = my_number_channels + 1

           my_num_channels = my_number_channels
        
           IF ((no_of_pes_per_sector > 1) .and. (i_am_outer_master .and. debug)) then
              rem2 = rem
              IF (rem == 0) rem2 = no_of_pes_per_sector
              print *, ' Outer region channel decomposition within each block_group_id:'
              print *, 'the first', rem2, ' pes have ', my_number_channels, 'channels,'
              print *, ' and the remaining', (no_of_pes_per_sector - rem2), 'pes have', &
                (my_number_channels - 1), ' channels.'
           END IF
           if (i_am_outer_master) print *, 'pe_for_channel(channel_id_1st:channel_id_last) =', &
                pe_for_channel(channel_id_1st:channel_id_last)

        ELSE   ! i am inner master or ground state inner task
           call get_my_pe_id(my_pe_id)
           ALLOCATE (disp_for_pe(0:no_of_pes_per_sector), &
                counts_per_pe(0:no_of_pes_per_sector), stat=ierr)
           call assert (ierr == 0, 'allocation problem in init_my_channels')
           my_number_channels = number_channels / no_of_pes_per_sector  ! add 1 to first rem pes below
           i_start = 1
           disp_for_pe(0) = 0  ! needed for gather/scatter between inner and outer region
           counts_per_pe(0) = 0
! disp_for_pe nd counts_per_pe are used in the 'first inner pe + first outer block' communicator           
           do i_pe = 0, rem - 1
              disp_for_pe(i_pe+1) = i_start - 1
              counts_per_pe(i_pe+1) = my_number_channels + 1
              i_fin = i_start + my_number_channels
              i_start = i_fin + 1
           end do   
           do i_pe = rem, no_of_pes_per_sector - 1
              i_fin = i_start + my_number_channels - 1
              disp_for_pe(i_pe+1) = i_start - 1
              counts_per_pe(i_pe+1) = my_number_channels
              i_start = i_fin + 1
           end do   

           my_number_channels = number_channels  ! reset
           my_num_channels = number_channels
           print *, 'I am ', my_pe_id, ' and disp_for_pe =', disp_for_pe
        END IF
    END SUBROUTINE init_my_channels

!-----------------------------------------------------------------------

    SUBROUTINE init_regions(my_post_dim_lim, my_post_for_tdse, r_at_region_bndry)

        INTEGER, INTENT(in)   :: my_post_dim_lim
        INTEGER, INTENT(OUT)  :: my_post_for_tdse(my_post_dim_lim)
        REAL(wp), INTENT(IN)  :: r_at_region_bndry

        IF (i_am_in_inner_region) THEN
            CALL init_inner_region(my_post_dim_lim, my_post_for_tdse)
        END IF

        IF (i_am_in_outer_region) THEN
            CALL init_outer_region(r_at_region_bndry)
        END IF

        IF (i_am_in_double_outer_region) THEN
            CALL init_two_electron_outer_region(r_at_region_bndry)
        END IF

    END SUBROUTINE init_regions

!-----------------------------------------------------------------------

    SUBROUTINE init_inner_region(my_post_dim_lim, my_post_for_tdse)

        USE calculation_parameters,   ONLY: nfdm, &
                                            krylov_hdt_desired, &
                                            delta_t, &
                                            krytech, &
                                            exptech
        USE inner_to_outer_interface, ONLY: alloc_psi_at_inner_fd_pts
        USE inner_parallelisation,    ONLY: setup_inner_region_parallelisation
        USE inner_propagators,        ONLY: setup_propagation_data
        USE mpi_layer_lblocks,        ONLY: i_am_block_master, inner_region_rank
        USE outer_to_inner_interface, ONLY: allocate_fderivs
        USE readhd,                   ONLY: states_per_LML_block
        USE setup_wv_data,            ONLY: setup_inner_region_wv

        INTEGER, INTENT(IN)   :: my_post_dim_lim 
        INTEGER, INTENT(OUT)  :: my_post_for_tdse(my_post_dim_lim)
        INTEGER, allocatable  :: my_post(:)
        integer               :: err

        CALL setup_inner_region_parallelisation(i_am_inner_master, i_am_in_outer_region, &
                                                states_per_LML_block, my_block_id)
!        print *, 'I am ', inner_region_rank, ' and  finished setup_inner '

        call assert (my_num_LML_blocks <= my_post_dim_lim, 'my_post_dim_lim is too small, reset in tdse')
        allocate (my_post(my_num_LML_blocks), stat=err)
        call assert (err == 0, 'allocate my_post error in initialise')

        CALL setup_inner_region_wv(i_am_inner_master, i_am_block_master, nfdm, my_post)
        my_post_for_tdse(1:my_num_LML_blocks) = my_post(1:my_num_LML_blocks)

        ! Prepare for time propagation
        CALL setup_propagation_data(propagation_order, &
                                    krytech, &
                                    exptech, &
                                    delta_t, krylov_hdt_desired, my_num_LML_blocks)
!        print *, 'I am ', inner_region_rank, ' and  finished propagator setup '

        ! Allocate array to hold data to pass from inner to outer region
        CALL alloc_psi_at_inner_fd_pts(nfdm)

        ! Allocate array to hold data passed to inner from outer region
        CALL allocate_fderivs

    END SUBROUTINE init_inner_region

!-----------------------------------------------------------------------

    SUBROUTINE init_outer_region(r_at_region_bndry)

        USE local_ham_matrix,   ONLY: init_local_ham_matrix_module
        USE lrpots,             ONLY: init_lrpots
        USE mpi_communications, ONLY: my_r_start_id
        USE outer_hamiltonian,  ONLY: test_outer_hamiltonian

        REAL(wp), INTENT(IN) :: r_at_region_bndry

        ! Only the outer region PEs need to initialize the wp range potentials:

        CALL init_lrpots

        CALL init_local_ham_matrix_module(r_at_region_bndry, &
                                          deltar, &
                                          my_r_start_id, i_am_outer_master, &
                                          i_am_in_first_outer_block, &
                                          i_am_in_last_outer_block)

        ! Tests the coupling relations used in the wp range potentials
        ! Must be called after init_lrpots and after init_ang_basis_set_params
        CALL test_outer_hamiltonian(i_am_outer_master)

    END SUBROUTINE init_outer_region

!-----------------------------------------------------------------------

    SUBROUTINE init_two_electron_outer_region(r_at_region_bndry)

        USE local_ham_matrix,   ONLY: init_local_ham_matrix_module
        USE lrpots,             ONLY: init_lrpots
        USE mpi_communications, ONLY: my_r_start_id
        USE two_electron_outer_hamiltonian,  ONLY: setup_region_three_neighbour_counts_and_store

        REAL(wp), INTENT(IN) :: r_at_region_bndry


        CALL init_lrpots

        CALL init_local_ham_matrix_module(r_at_region_bndry, &
                                          deltar, &
                                          my_r_start_id, i_am_outer_master, & 
                                          i_am_in_first_outer_block, &
                                          i_am_in_last_outer_block)

        ! Tests the coupling relations of the long-range potentials
        ! in the two-electron outer region
        CALL setup_region_three_neighbour_counts_and_store

    END SUBROUTINE init_two_electron_outer_region

!-----------------------------------------------------------------------

    SUBROUTINE r_point_check(spoint, r_point) ! Molecular

        IMPLICIT NONE

        REAL(wp), INTENT(IN) :: spoint, r_point

        IF (ABS(spoint - r_point) > 10e-12_wp) THEN
            PRINT *, 'R_point_check: ', spoint, r_point
            CALL assert(.false., 'R_point_check failed: inconsistent value of r_point!')
        END IF

    END SUBROUTINE r_point_check

END MODULE initialise
