! Copyright 2023
!
! This file is part of RMT.
!
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
!
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
!
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief Small functions that don't fit anywhere else.
MODULE rmt_utilities
    USE precisn, ONLY: wp

    PRIVATE

    PUBLIC int_to_char, real_to_es_char
CONTAINS
    !> \brief Calcaulate how many characters the textual representation of a base-10 integer would occupy.
    PURE FUNCTION width(n)
        INTEGER :: width
        INTEGER, INTENT(in) :: n

        IF (n == 0) THEN
            width = 1
        ELSE
            width = INT(FLOOR(LOG10(REAL(ABS(n))))) + 1
            ! To account for the negative sign.
            IF (n < 0) THEN
                width = width + 1
            END IF
        END IF
    END FUNCTION

    !> \brief Convert an integer to its base-10 string representation without padding.
    PURE FUNCTION int_to_char(n) RESULT(s)
        INTEGER, INTENT(IN) :: n
        CHARACTER(len=:), ALLOCATABLE :: s

        INTEGER :: str_width, i, remaining, digit

        str_width = width(n)
        ALLOCATE (CHARACTER(len=str_width) :: s)
        WRITE (s, '(I0)') n
    END FUNCTION

    !> \brief Convert a real number to it's base-10 string representation without padding.
    PURE FUNCTION real_to_es_char(r, PRECISION) RESULT(s)
        REAL(wp), INTENT(IN) :: r
        INTEGER, INTENT(IN), OPTIONAL :: PRECISION
        CHARACTER(len=:), ALLOCATABLE :: s

        INTEGER :: prec, str_width
        CHARACTER(len=:), ALLOCATABLE :: fmt

        IF (PRESENT(PRECISION)) THEN
            prec = PRECISION
        ELSE
            prec = 6
        END IF

        str_width = prec + 7
        ALLOCATE (CHARACTER(len=str_width) :: s)

        fmt = "(ES"//int_to_char(str_width)//"."//int_to_char(prec)//")"
        WRITE (s, fmt) r
    END FUNCTION
END MODULE
