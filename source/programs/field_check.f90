! Copyright 2018 
!
! This file is part of RMT.
! 
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
! 
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
! 
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit 
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup utilities
!> @brief Program for calculating the electric field outside of a full RMT calculation.
!> (Useful for checking)

PROGRAM field_check

    USE precisn,                ONLY: wp
    USE initial_conditions,     ONLY: read_initial_conditions, &
                                      get_disk_path,&
                                      dipole_output_desired,&
                                      timesteps_per_output, &
                                      no_of_field_confs
    USE calculation_parameters, ONLY: steps_per_run, &
                                      delta_t, &
                                      derive_calculation_parameters
    USE electric_field,         ONLY: get_E_pulse, init_electric_field_module
    USE version_control,        ONLY: print_program_header
    USE io_files,               ONLY: write_output_files
    USE mpi_communications,     ONLY: check_initial_field

    IMPLICIT NONE

    REAL(wp)  :: iteration_start_time
    REAL(wp)  :: iteration_finish_time
    REAL(wp)  :: field_evaluation_time
    REAL(wp), ALLOCATABLE  :: current_E_field_strength(:, :)
    INTEGER   :: timeindex, i, stride

    CALL print_program_header

    CALL get_disk_path('')

    CALL read_initial_conditions

    CALL init_electric_field_module

    CALL derive_calculation_parameters

    ALLOCATE (current_E_field_strength(3, no_of_field_confs))

    stride = MERGE(1, timesteps_per_output, dipole_output_desired)

    OPEN (UNIT=46,FILE='EField.test',STATUS='unknown')

    DO timeindex = 0, steps_per_run - 1, stride

        iteration_start_time = timeindex*delta_t
        iteration_finish_time = iteration_start_time + delta_t
        field_evaluation_time = iteration_start_time + 0.50_wp*delta_t

        DO i = 1, no_of_field_confs
            current_E_field_strength(:, i) = get_E_pulse(field_evaluation_time, i)
        END DO

        IF (timeindex == 0) THEN
            call check_initial_field(current_E_field_strength, .FALSE.)
        END IF
        CALL write_output_files(iteration_start_time, iteration_finish_time, current_E_field_strength)

    END DO

    CLOSE(46)

END PROGRAM field_check
